//
//  PaymentHistoryViewcontroller.swift
//  TechGo
//
//  Created by Thiện Nguyễn on 21/12/2020.
//
import Foundation
import UIKit

enum PaymentType {
	case withdraw, charge
}

struct PaymentData {
	var amount: CGFloat
	var id: String
	var type: PaymentType
}
class PaymentHistoryViewcontroller: CustomViewcontroller, LoginUIProtocol {
    
	var dataList: [PaymentData] =  [
		PaymentData(amount: 100000, id: "123", type: .withdraw),
		PaymentData(amount: 200000, id: "123", type: .withdraw),
		PaymentData(amount: 120000, id: "123", type: .withdraw),
		PaymentData(amount: 140000, id: "123", type: .charge),
		PaymentData(amount: 410000, id: "123", type: .charge),
		PaymentData(amount: 230000, id: "123", type: .withdraw),
		PaymentData(amount: 810000, id: "123", type: .withdraw),
		PaymentData(amount: 100000, id: "123", type: .withdraw),
		PaymentData(amount: 100000, id: "123", type: .withdraw),
		PaymentData(amount: 100000, id: "123", type: .charge),
		PaymentData(amount: 100000, id: "123", type: .withdraw),
		PaymentData(amount: 100000, id: "123", type: .withdraw),
		PaymentData(amount: 100000, id: "123", type: .withdraw),
		PaymentData(amount: 100000, id: "123", type: .charge),
		PaymentData(amount: 100000, id: "123", type: .withdraw),
		PaymentData(amount: 100000, id: "123", type: .charge),
	]
	
	var activityHistoryTableview = UITableView()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.view.backgroundColor = .lighterPrimaryColor()
		let contentView = UIView().then({
			$0.layer.cornerRadius = 16
			$0.clipsToBounds = true
		})
		self.addLogoImg()

		self.view.addSubview(contentView)
		contentView.addSubview(titleLabel)
		contentView.addSubview(activityHistoryTableview)
		
		activityHistoryTableview.then({
			$0.layer.cornerRadius = 16
			$0.clipsToBounds = true
			$0.showsVerticalScrollIndicator = false
			$0.delegate = self
			$0.dataSource = self
			$0.register(PaymentHistoryTableViewCell.self , forCellReuseIdentifier: "cell")
			$0.estimatedRowHeight = 50.0
			
		})
		edgesForExtendedLayout = []
		titleLabel.snp.makeConstraints({
			$0.left.top.right.equalToSuperview()
			$0.height.equalTo(50)
			$0.bottom.equalTo(activityHistoryTableview.snp.top).offset(-topPadding/2)
		})
		activityHistoryTableview.snp.makeConstraints({
			$0.left.right.bottom.equalToSuperview()
		})
		contentView.snp.makeConstraints({
			$0.top.equalTo(self.logoImgView.snp.bottom).offset(topPadding)
			$0.left.right.bottom.equalToSuperview().inset(topPadding)
		})
	}
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.titleLabel.text != "payment_history".localized() {
            self.titleLabel.text = "payment_history".localized()
            self.activityHistoryTableview.reloadData()
            self.tabBarItem.title = "payment".localized()
        }
        
    }
	
	var titleLabel = UILabel().then({
		$0.backgroundColor = .white
		$0.textAlignment = .center
		$0.text = "payment_history".localized()
		$0.font = UIFont.systemFont(ofSize: 20, weight: .medium)
		$0.layer.cornerRadius = 16
		$0.clipsToBounds = true
	})
}

extension PaymentHistoryViewcontroller: UITableViewDelegate, UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return dataList.count
	}
	
	func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
		return UITableView.automaticDimension
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PaymentHistoryTableViewCell
		cell.paymentData = dataList[indexPath.row]
		return cell
	}
	
}
