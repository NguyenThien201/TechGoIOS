//
//  ActivityViewController.swift
//  TechGo
//
//  Created by Thiện Nguyễn on 20/12/2020.
//

import Foundation
import UIKit
enum TripType {
	case bike, car
}
struct ActivityData {
	var from: String //location: Pos(lon: lat: )
	var to: String //location: Pos(lon: lat: )
	var cost: CGFloat
	var id: String
	var driverName: String
	var customerName: String
	var type: TripType
}

class ActivityViewController: CustomViewcontroller, LoginUIProtocol {
	var dataList: [ActivityData] =  [
		ActivityData(from: "ABc", to: "235/95/31 Nam Kỳ Khởi Nghĩa, phường 7, Quận 3, thành phố Hồ Chí Minh", cost: 100000, id: "trip123", driverName: "Nguyenvana", customerName: "NguyenVanB", type: .bike),
		ActivityData(from: "ABc", to: "235/95/31 Nam Kỳ Khởi Nghĩa, phường 7, Quận 3, thành phố Hồ Chí Minh", cost: 100000, id: "trip123", driverName: "Nguyenvana", customerName: "NguyenVanB", type: .car),
		ActivityData(from: "ABc", to: "235/95/31 Nam Kỳ Khởi Nghĩa, phường 7, Quận 3, thành phố Hồ Chí Minh", cost: 100000, id: "trip123", driverName: "Nguyenvana", customerName: "NguyenVanB", type: .bike),
		ActivityData(from: "ABc", to: "235/95/31 Nam Kỳ Khởi Nghĩa, phường 7, Quận 3, thành phố Hồ Chí Minh", cost: 100000, id: "trip123", driverName: "Nguyenvana", customerName: "NguyenVanB", type: .car),
		ActivityData(from: "ABc", to: "235/95/31 Nam Kỳ Khởi Nghĩa, phường 7, Quận 3, thành phố Hồ Chí Minh", cost: 100000, id: "trip123", driverName: "Nguyenvana", customerName: "NguyenVanB", type: .bike),
		ActivityData(from: "ABc", to: "235/95/31 Nam Kỳ Khởi Nghĩa, phường 7, Quận 3, thành phố Hồ Chí Minh", cost: 100000, id: "trip123", driverName: "Nguyenvana", customerName: "NguyenVanB", type: .car),
		ActivityData(from: "ABc", to: "235/95/31 Nam Kỳ Khởi Nghĩa, phường 7, Quận 3, thành phố Hồ Chí Minh", cost: 100000, id: "trip123", driverName: "Nguyenvana", customerName: "NguyenVanB", type: .bike),
		ActivityData(from: "ABc", to: "235/95/31 Nam Kỳ Khởi Nghĩa, phường 7, Quận 3, thành phố Hồ Chí Minh", cost: 100000, id: "trip123", driverName: "Nguyenvana", customerName: "NguyenVanB", type: .car),]
	
	var activityHistoryTableview = UITableView()
    
    var titleLabel = UILabel().then({
        $0.backgroundColor = .white
        $0.textAlignment = .center
        $0.text = "activity_history".localized()
        $0.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        $0.layer.cornerRadius = 16
        $0.clipsToBounds = true
    })
    
	override func viewDidLoad() {
		super.viewDidLoad()
		self.view.backgroundColor = .lighterPrimaryColor()				
		let contentView = UIView().then({
			$0.layer.cornerRadius = 16
			$0.clipsToBounds = true
		})
		
		self.addLogoImg()
		self.view.addSubview(contentView)
		contentView.addSubview(titleLabel)
		contentView.addSubview(activityHistoryTableview)
						
		activityHistoryTableview.then({
			$0.layer.cornerRadius = 16
			$0.clipsToBounds = true
			$0.showsVerticalScrollIndicator = false
			$0.delegate = self
			$0.dataSource = self
			$0.register(ActivityHistoryTableViewCell.self , forCellReuseIdentifier: "cell")
			$0.estimatedRowHeight = 50.0
			
		})
		
		edgesForExtendedLayout = []
		titleLabel.snp.makeConstraints({
			$0.left.top.right.equalToSuperview()
			$0.height.equalTo(50)
			$0.bottom.equalTo(activityHistoryTableview.snp.top).offset(-topPadding/2)
		})
		
		activityHistoryTableview.snp.makeConstraints({
			$0.left.right.bottom.equalToSuperview()
		})
		
		contentView.snp.makeConstraints({
			$0.top.equalTo(self.logoImgView.snp.bottom).offset(topPadding)
			$0.left.right.bottom.equalToSuperview().inset(topPadding)
		})
	}

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.titleLabel.text != "activity_history".localized() {
            self.titleLabel.text = "activity_history".localized()
            self.activityHistoryTableview.reloadData()
            self.tabBarItem.title = "activity".localized()
        }
    }
}

extension ActivityViewController: UITableViewDelegate, UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return dataList.count
	}
	
	func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
		return UITableView.automaticDimension
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ActivityHistoryTableViewCell
		cell.activityData = dataList[indexPath.row]
		return cell
	}
	
}
