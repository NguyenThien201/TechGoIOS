//
//  HomeMenuViewController.swift
//  TechGo
//
//  Created by FRIDAY on 15/12/2020.
//

import Foundation
import MaterialComponents

class HomeMenuViewController: CustomViewcontroller, LoginUIProtocol {
    let bottomNavBar = MDCBottomNavigationBar()
    var navbarHeight: CGFloat = 50
    var balanceBtn = MDCButton()
    var walletBtn = MDCButton()
    var bookTripbtn = MDCButton()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tabBarItem.title = "home".localized()
      
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        UserData.shared.fetchingData({
            if UserData.shared.userType == .driver {
                DriverTripDataHandler.shared.beginCheckTrip(action: {
                    let createNewVc = CustomerAcceptViewController()
                    createNewVc.modalPresentationStyle = .fullScreen
                    self.performPresentAnimation()
                    self.present(createNewVc,
                                 animated: false,
                                 completion: {self.removeAllLayerAnimation()})
                })
            }
            if UserData.shared.userType == UserType.driver {
                self.bookTripbtn.setTitle("waiting_for_a_trip".localized(), for: .normal)
            } else {
                self.bookTripbtn.setTitle("book_a_trip".localized(), for: .normal)
            }
            
            self.balanceBtn.setTitle("\(UserData.shared.wallet)  vnd", for: .normal)
        })
    }
    override func viewDidLoad() {
       
      
        super.viewDidLoad()
        self.view.backgroundColor = .lighterPrimaryColor()
        self.addLogoImg()
        holderView = self.configHolderView()
        holderView.backgroundColor = .clear
        holderView.layer.borderWidth = 0
        addButton()
       
    }
    
    var color1 = 0xe6e66c
    var color2 = 0xffffab
    
    private func addButton() {
        configButton()
        balanceBtn.backgroundColor = .primaryColor()//applyGradient(colours: [.primaryColor(),.primaryColor()] )
        
        walletBtn.backgroundColor = .primaryColor()//(colours: [.primaryColor(), .primaryColor()])
        bookTripbtn.backgroundColor = UIColor(rgb: 0xababff)//applyGradient(colours: [UIColor(rgb: 0xababff), UIColor(rgb: 0xababff)])
        self.holderView.addSubview(balanceBtn)
        self.holderView.addSubview(walletBtn)
        self.holderView.addSubview(bookTripbtn)
        
        balanceBtn.snp.makeConstraints({
            $0.left.top.equalToSuperview()
            $0.height.equalTo(balanceBtn.snp.width)
        })
        walletBtn.snp.makeConstraints({
            $0.right.top.equalToSuperview()
            $0.left.equalTo(balanceBtn.snp.right).offset(topPadding)
            $0.width.height.equalTo(balanceBtn.snp.width)
        })
        
        bookTripbtn.snp.makeConstraints({
            $0.height.equalTo(walletBtn.snp.height)
            $0.top.equalTo(walletBtn.snp.bottom).offset(topPadding)
            $0.left.right.bottom.equalToSuperview()
        })
        self.holderView.layoutIfNeeded()
    }
    
    func configButton() {
        
        self.balanceBtn.then({
            $0.isUppercaseTitle = false
            $0.clipsToBounds = true
            $0.titleLabel?.numberOfLines = -1
            $0.layer.cornerRadius = 25
            
            $0.setTitle("balance".localized()+"\n0000 vnd", for: .normal)
            
            $0.setTitleFont(UIFont.systemFont(ofSize: 20, weight: .semibold), for: .normal)
            //                $0.addTarget(self, action: #selector(diverSelected), for: .touchUpInside)
        })
        self.walletBtn.then({
            $0.isUppercaseTitle = false
            $0.clipsToBounds = true
            $0.layer.cornerRadius = 25
            $0.setTitle("wallet".localized(), for: .normal)
            $0.setTitleFont(UIFont.systemFont(ofSize: 25, weight: .semibold), for: .normal)
            //                $0.addTarget(self, action: #selector(customerSelected), for: .touchUpInside)
        })
        
        self.bookTripbtn.then({
            $0.clipsToBounds = true
            $0.layer.cornerRadius = 25
            if UserData.shared.userType == UserType.driver {
                $0.setTitle("waiting_for_a_trip".localized(), for: .normal)
            } else {
                $0.setTitle("book_a_trip".localized(), for: .normal)
            }
            
            $0.setTitleFont(UIFont.systemFont(ofSize: 25, weight: .semibold), for: .normal)
            $0.addTarget(self, action: #selector(bookTripDidClicked), for: .touchUpInside)
        })
    }
    
    @objc func bookTripDidClicked() {
        //     let vc = BookingViewController
        if UserData.shared.userType == .customer {
            let createNewVc = BookingViewController()
            createNewVc.modalPresentationStyle = .fullScreen
            self.performPresentAnimation()
            self.present(createNewVc,
                         animated: false,
                         completion: {self.removeAllLayerAnimation()})
            
        } else {
            
        }
    }
    
    override func updateTextFieldLanguage() {
        self.bookTripbtn.then({
            if UserData.shared.userType == UserType.driver {
                $0.setTitle("waiting_for_a_trip".localized(), for: .normal)
            } else {
                $0.setTitle("book_a_trip".localized(), for: .normal)
            }
        })
        self.walletBtn.setTitle("wallet".localized(), for: .normal)
        self.balanceBtn.setTitle("balance".localized()+"\n100.000 vnd", for: .normal)
    }
    
    
}
