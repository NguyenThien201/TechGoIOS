//
//  AccountSettingViewController.swift
//  TechGo
//
//  Created by FRIDAY on 21/12/2020.
//

import Foundation
import UIKit
import SnapKit
import MaterialComponents
import NVActivityIndicatorView
import Firebase
import FirebaseAuth
import FirebaseDatabase

class AccountSettingViewController: CustomViewcontroller, LoginUIProtocol {
	let imgSize: CGFloat = 80
	var dataList: [String] =  [
		"changePassword",
		"setting",
		"logout",
	]
	var userNameLbl = UILabel().then({
		$0.font = UIFont.systemFont(ofSize: 25, weight: .medium)
        $0.text = "fetching_name".localized()
        $0.textColor = UIColor.lightGray.withAlphaComponent(0.5)
		$0.textAlignment = .center
	})
	var editProfileLbl = UILabel().then({
		$0.font = UIFont.systemFont(ofSize: 18, weight: .medium)
		$0.text = "editProfile".localized()
		$0.textColor = .lightGray
		$0.textAlignment = .right
	})
	var activityHistoryTableview = UITableView()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.view.backgroundColor = .lighterPrimaryColor()
				
		let contentView = UIView().then({
			$0.layer.cornerRadius = 16
			$0.clipsToBounds = true
		})
		self.view.addSubview(contentView)
		self.addLogoImg()

		contentView.addSubview(self.accountInfoView)
		contentView.addSubview(activityHistoryTableview)
		
		activityHistoryTableview.then({			
			$0.alwaysBounceVertical = false
			$0.layer.cornerRadius = 16
			$0.clipsToBounds = true
			$0.showsVerticalScrollIndicator = false
			$0.delegate = self
			$0.dataSource = self
			$0.register(AccountSettingTableViewCell.self , forCellReuseIdentifier: "cell")
			$0.estimatedRowHeight = 100.0
			
		})
		edgesForExtendedLayout = []
		
		accountInfoView.addSubview(accountImgView)
		
		accountImgView.snp.makeConstraints({		
			$0.width.height.equalTo(imgSize)
			$0.left.top.bottom.equalToSuperview().inset(10)
		})
		
		accountInfoView.addSubview(userNameLbl)
		userNameLbl.snp.makeConstraints({
			$0.left.equalTo(accountImgView.snp.right).offset(10)
			$0.top.right.equalToSuperview().inset(10)
		})
		accountInfoView.addSubview(editProfileLbl)
		let arrowIcon = UIImageView(image: UIImage(named: "baseline_navigate_next_white_48pt"))
		accountInfoView.addSubview(arrowIcon)
		arrowIcon.snp.makeConstraints({
			$0.right.equalToSuperview().inset(10)
			$0.width.height.equalTo(40)
			$0.left.equalTo(editProfileLbl.snp.right).offset(10)
			$0.centerY.equalTo(editProfileLbl.snp.centerY)
		})
		editProfileLbl.snp.makeConstraints({
			$0.top.equalTo(userNameLbl.snp.bottom).offset(20)
			$0.left.equalTo(accountImgView.snp.right).offset(10)
			$0.bottom.equalToSuperview().inset(10)
		})
		
		accountInfoView.snp.makeConstraints({
			$0.left.top.right.equalToSuperview()
			//			$0.height.equalTo(150)
			$0.bottom.equalTo(activityHistoryTableview.snp.top).offset(-topPadding/2)
		})
		
		activityHistoryTableview.snp.makeConstraints({
			$0.left.right.equalToSuperview()
			$0.height.equalTo(300)
		})
		contentView.snp.makeConstraints({ 
			$0.top.equalTo(self.logoImgView.snp.bottom).offset(topPadding)
			$0.left.right.bottom.equalToSuperview().inset(topPadding)
		})
		loadUserData()
	}
    
    func loadUserData() {
        let ref = Database.database().reference()
        if let user = Auth.auth().currentUser {
            ref.child("users").child(user.uid).observeSingleEvent(of: .value, with: {(snaps) in
                guard let value = snaps.value as? NSDictionary else {
                    return
                }
                let displayName = value["displayName"] ?? ""
                self.userNameLbl.text = value["displayName"] as? String ?? ""
                self.userNameLbl.textColor = .black
            })
//            self.userNameLbl.text = user.
        }
    }

	
	var accountInfoView = UIView().then({ 
		$0.backgroundColor = .white
		$0.layer.cornerRadius = 16
		$0.clipsToBounds = true
	})
	
	lazy var accountImgView = UIImageView().then({
		$0.clipsToBounds = true		
		$0.layer.cornerRadius = CGFloat(imgSize/2)
		$0.layer.borderWidth = 5.0
		$0.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
		$0.clipsToBounds = true
		$0.contentMode = .scaleAspectFill		
		$0.image = UIImage(named: "lisa")
	})
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		print(LangSetting.lang.rawValue)
		self.activityHistoryTableview.reloadData()
	}
	func presentLogoutDialog() {
		let alertController = MDCAlertController(title: "Logout".localized(), message: "do_you_want_to_logging_out".localized())
		let action = MDCAlertAction(title:"Cancel".localized().uppercased()) { _ in alertController.dismiss(animated: true, completion: {}) }
		func presentLoginVc() {
			let vc = LoginViewController()
			vc.modalPresentationStyle = .fullScreen
			self.performPresentAnimation()
			self.present(vc, animated: false, completion: {
				self.removeAllLayerAnimation()
				
			})
		}
		let actionConfirm = MDCAlertAction(title:"Confirm".localized().uppercased()) { _ in alertController.dismiss(animated: true, completion: {
			if let window = UIApplication.shared.windows.first { 
				let animateView = UIView(frame: window.bounds).then({
					$0.backgroundColor = .black
					$0.alpha = 0.4
				})
				window.addSubview(animateView);
				animateView.snp.makeConstraints({
					$0.edges.equalToSuperview()
				})
				
				let animate = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 80, height: 80), type: .ballRotateChase, color: .primaryColor(), padding: 5)
				animateView.addSubview(animate)
				animate.snp.makeConstraints({$0.center.equalToSuperview()})
				animate.startAnimating()
                do {
                    try Auth.auth().signOut()
                } catch {
                    
                }
                
				DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
					animate.stopAnimating()
					animateView.removeFromSuperview()
					presentLoginVc()
				}) 										
			} else { 
				presentLoginVc()
			}
		})
		}
		alertController.then({
			$0.titleFont = UIFont.systemFont(ofSize: 20, weight: .medium)
			$0.buttonTitleColor = .primaryColor()
			$0.addAction(actionConfirm)
			$0.addAction(action)
			$0.cornerRadius = 8
		})
		present(alertController, animated:true, completion: {})
	}
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tabBarItem.title = "account".localized()
        self.editProfileLbl.text = "editProfile".localized()
        
    }
    
}

extension AccountSettingViewController: UITableViewDelegate, UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return dataList.count
	}
	
	func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
		return UITableView.automaticDimension
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AccountSettingTableViewCell
		cell.infoLabel.text = dataList[indexPath.row].localized()
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		switch dataList[indexPath.row]  {
			case "setting":
				let vc = SettingViewController().then({
					$0.modalPresentationStyle = .fullScreen
				})
				self.performPresentAnimation()
				self.present(vc, animated: false, completion: {})
				break
			case "logout":
				self.presentLogoutDialog()
				break
			default:
				let vc = ResetPasswordViewController().then({
					$0.modalPresentationStyle = .fullScreen
					$0.isBackToSignIn = false
				})
				self.performPresentAnimation()
				self.present(vc, animated: false, completion: {})
				break
		}
	}
}
