//
//  SettingViewController.swift
//  TechGo
//
//  Created by FRIDAY on 21/12/2020.
//

import Foundation
import UIKit
import MaterialComponents

class SettingViewController: CustomViewcontroller, LoginUIProtocol {
	
	let languageButton = MDCButton().then({
		$0.setTitle(LangSetting.getLangText(), for: .normal)
		$0.backgroundColor = .primaryColor()
		$0.addTarget(self, action: #selector(langDidClicked), for: .touchUpInside)
		$0.setTitleFont(UIFont.systemFont(ofSize: 20, weight: .semibold), for: .normal)
	})
	
	
	let changeLanguageLabel = UILabel().then({
		$0.backgroundColor = .white
		$0.textAlignment = .left
		$0.text = "change_language".localized()
		$0.font = UIFont.systemFont(ofSize: 25, weight: .medium)
	})
	
	let setPinCodeLabel = UILabel().then({	
		$0.textAlignment = .left
		$0.text = "set_pincode".localized()
		$0.font = UIFont.systemFont(ofSize: 25, weight: .medium)
	})
	
	override func viewDidLoad() {		
		super.viewDidLoad()
		self.addBackButton()
		self.addLogoImg()
		self.view.backgroundColor = .lighterPrimaryColor()
		
		let contentView = UIView().then({
			$0.layer.cornerRadius = 16
			$0.clipsToBounds = true
		})
		
		self.view.addSubview(contentView)
		let pincodeView = UIView().then({
			$0.layer.cornerRadius = 16
			$0.clipsToBounds = true
			$0.backgroundColor = .white
		})
		
		let languageView = UIView().then({
			$0.layer.cornerRadius = 16
			$0.clipsToBounds = true
			$0.backgroundColor = .white
		})
		
		
		contentView.then({
			$0.addSubview(titleLabel)
			$0.addSubview(pincodeView)
			$0.addSubview(languageView)			
		})
		
		
		
		titleLabel.snp.makeConstraints({
			$0.left.top.right.equalToSuperview()
			$0.height.equalTo(50)
			$0.bottom.equalTo(languageView.snp.top).offset(-topPadding/2)
		})
		
		pincodeView.addSubview(self.setPinCodeLabel)
		setPinCodeLabel.snp.makeConstraints({
			$0.left.equalToSuperview().inset(30)
			$0.centerY.equalToSuperview()
		})
		let arrowIcon = UIImageView(image: UIImage(named: "baseline_navigate_next_white_48pt"))
		pincodeView.addSubview(arrowIcon)
		arrowIcon.snp.makeConstraints({
			$0.right.equalToSuperview().inset(10)
			$0.width.height.equalTo(40)
			$0.left.equalTo(setPinCodeLabel.snp.right).offset(10)
			$0.centerY.equalToSuperview()
		})
		pincodeView.snp.makeConstraints({
			$0.left.right.equalToSuperview()			
			$0.height.equalTo(100)			
			$0.bottom.equalToSuperview()
		})
		
		languageView.addSubview(self.changeLanguageLabel)
		languageView.addSubview(self.languageButton)
		languageButton.snp.makeConstraints({
			$0.right.equalToSuperview().inset(topPadding)
			$0.width.equalTo(150)
			$0.centerY.equalToSuperview()
		})
		changeLanguageLabel.snp.makeConstraints({
			$0.left.equalToSuperview().inset(30)
			$0.centerY.equalToSuperview()
		})
		languageView.snp.makeConstraints({
			$0.left.right.equalToSuperview()			
			$0.height.equalTo(100)
			$0.bottom.equalTo(pincodeView.snp.top).offset(-topPadding/2)
		})
		
		contentView.snp.makeConstraints({ 
			$0.centerY.equalToSuperview()
			$0.left.right.equalToSuperview().inset(topPadding)
		})
		
	}
	
	@objc 
	func langDidClicked() { 
		LangSetting.changeLange()
		languageButton.setTitle(LangSetting.getLangText(), for: .normal)
		setPinCodeLabel.text = "set_pincode".localized()
		titleLabel.text = "setting".localized()		
		changeLanguageLabel.text = "change_language".localized()
		self.backButton.setTitle("back".localized(), for: .normal)
	}
	
	var titleLabel = UILabel().then({
		$0.backgroundColor = .white
		$0.textAlignment = .center
		$0.text = "setting".localized()
		$0.font = UIFont.systemFont(ofSize: 20, weight: .medium)
		$0.layer.cornerRadius = 16
		$0.clipsToBounds = true
	})
	
}
