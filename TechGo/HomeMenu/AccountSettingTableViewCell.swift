//
//  AccountSettingTableViewCell.swift
//  TechGo
//
//  Created by FRIDAY on 21/12/2020.
//

import Foundation
import UIKit
import SnapKit

class AccountSettingTableViewCell: UITableViewCell {

	var infoLabel = UILabel().then({
		$0.font = UIFont.systemFont(ofSize: 25, weight: .medium)
		$0.textColor = .darkGray
		$0.textAlignment = .left
	})
	
	var img = UIImageView(image: UIImage(named: "baseline_navigate_next_white_48pt"))
	
	override func didMoveToSuperview() {
		super.didMoveToSuperview()
		self.addSubview(img)
		self.selectionStyle = .none
		img.snp.makeConstraints({
			$0.right.equalToSuperview().inset(8)
			$0.centerY.equalToSuperview()
			$0.width.height.equalTo(40)
		})
		self.addSubview(infoLabel)
		infoLabel.numberOfLines = -1
		infoLabel.snp.makeConstraints({
			$0.left.equalToSuperview().inset(30)
			$0.top.bottom.equalToSuperview()
			$0.height.equalTo(100)
			$0.right.equalTo(img.snp.left).offset(8)			
		})


		self.separatorInset = UIEdgeInsets.zero
	}	
	
}
