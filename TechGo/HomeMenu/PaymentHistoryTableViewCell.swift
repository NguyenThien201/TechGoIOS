//
//  PaymentHistoryTableViewCell.swift
//  TechGo
//
//  Created by Thiện Nguyễn on 21/12/2020.
//

import Foundation
import UIKit
import SnapKit

class PaymentHistoryTableViewCell: UITableViewCell {
    var paymentData: PaymentData? = nil {
        didSet {
            self.configCell()
        }
    }
    var infoLabel = UILabel()
    var img = UIImageView()
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        self.addSubview(img)
		self.selectionStyle = .none
        img.snp.makeConstraints({
//            $0.left.top.bottom.equalToSuperview().inset(4)
            $0.left.equalToSuperview().inset(20)
            $0.centerY.equalToSuperview()
            $0.width.height.equalTo(40)
        })
        self.addSubview(infoLabel)
        infoLabel.numberOfLines = -1
        infoLabel.snp.makeConstraints({
            $0.top.right.bottom.equalToSuperview().inset(8)
            $0.left.equalTo(img.snp.right).offset(20)
        })
        self.separatorInset = UIEdgeInsets.zero
    }
    
    private func configCell() {
        guard let data = self.paymentData else { return }
        self.img.image = UIImage(named: data.type == PaymentType.withdraw ? "icons8-refund-64.png" : "icons8-wallet-64")
        self.infoLabel.text = (data.type == PaymentType.withdraw ? "withdraw".localized() : "charge".localized()) + " :\n\(data.amount)vnd"
    }
    
}
