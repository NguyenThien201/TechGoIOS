//
//  LoginUIProtocol.swift
//  TechGo
//
//  Created by Thiện Nguyễn on 19/12/2020.
//

import Foundation
import MaterialComponents
import Then

protocol LoginUIProtocol {
    
}
extension LoginUIProtocol where Self: UITextFieldDelegate {
    
    func getTextField(with placeHolder: String) -> MDCOutlinedTextField {
        return MDCOutlinedTextField(frame: .zero).then({
            //            $0.textRect(forBounds: self.view.frame)
            $0.backgroundColor = .white
            $0.placeholder = placeHolder
            $0.delegate = self
            $0.tintColor = .primaryColor()
            $0.setOutlineColor(.primaryColor() , for: .editing)
            $0.sizeToFit()
            $0.clearsOnBeginEditing = false
        })
    }
    
    
}
extension LoginUIProtocol where Self: UIViewController {
    
    var topPadding: CGFloat { return 20.0 }
    func configHolderView() -> UIView {
        let holderView = UIView().then({
            $0.backgroundColor = .white
            $0.layer.cornerRadius = 6
            $0.layer.borderWidth = 1.0
            $0.layer.borderColor = UIColor.gray.cgColor
        })
        self.view.addSubview(holderView)
        
        holderView.snp.makeConstraints({
            $0.center.equalToSuperview()
            $0.left.right.equalToSuperview().inset(20)
            
        })
        return holderView
    }
    
    func configTitleLabel(_ title: String) -> UILabel {
        return UILabel().then({
            $0.text = title
            $0.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
            $0.textAlignment = .center
        })
    }
    
    func configVisibleButton() -> UIButton {
       return UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25)).then({
            $0.setImage(UIImage(named: "visible")?.withTintColor(.lightGray), for: .normal)
            $0.setImage(UIImage(named: "invisible")?.withTintColor(.lightGray), for: .selected)
            $0.isSelected = true
        })
    }
    
}
