//
//  NextSignUpViewController.swift
//  TechGo
//
//  Created by Thiện Nguyễn on 19/12/2020.
//

import Foundation
import SnapKit
import MaterialComponents
import Then
import Firebase
import FirebaseAuth
import FirebaseDatabase

class NextSignUpViewController: CustomViewcontroller, LoginUIProtocol, DialogUIProtocol {
    var signupUserData = UserData()

    var previousBtn = MDCButton()
    var finishBtn = MDCButton()
    
    var checkedBox = UIButton().then({
        $0.tintColor = .lighterPrimaryColor()
        $0.addTarget(self, action: #selector(checkedBoxSelected), for: .touchUpInside)
        $0.setImage(UIImage(named: "round_check_box_white_48pt"), for: .selected)
        $0.setImage(UIImage(named: "round_check_box_outline_blank_white_48pt"), for: .normal)
    })
    
    var displayNameLbl: MDCOutlinedTextField!
//    var address: MDCOutlinedTextField!
//    var dobLbl: MDCOutlinedTextField!
    
    var bikeButton = MDCButton().then({
        $0.setTitle("bike".localized(), for: .normal)
        $0.setTitleFont(UIFont.systemFont(ofSize: 15, weight: .semibold), for: .normal)
        $0.setBackgroundColor(.lighterPrimaryColor(), for: .normal)
        $0.setBackgroundColor(.primaryColor(), for: .selected)
//        $0.layer.cornerRadius = 16
        
        $0.addTarget(self, action: #selector(bikeDidClick), for: .touchUpInside)
    })
    var carButton = MDCButton().then({
        $0.setTitle("car".localized(), for: .normal)
        $0.setTitleFont(UIFont.systemFont(ofSize: 15, weight: .semibold), for: .normal)
        $0.setBackgroundColor(.lighterPrimaryColor(), for: .normal)
        $0.setBackgroundColor(.primaryColor(), for: .selected)
//        $0.layer.cornerRadius = 16
        $0.addTarget(self, action: #selector(carDidClick), for: .touchUpInside)
    })
    @objc
    func bikeDidClick() {
        self.carButton.isSelected = false
        self.bikeButton.isSelected = true
        
    }
    
    
    @objc
    func carDidClick() {
        self.carButton.isSelected = true
        self.bikeButton.isSelected = false
//        tripData.vehicleType = .car
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addBackButton()
        self.addLanguageButton()
		self.addLogoImg()
        self.view.backgroundColor = .lighterPrimaryColor()
        holderView = self.configHolderView()
        
		
        let titleLabel = self.configTitleLabel("signup".localized())
        let termLbl = UILabel().then({
            $0.numberOfLines = -1
            $0.font = UIFont.systemFont(ofSize: 15, weight: .regular)
            $0.text = "agree_with_our_term_of_service".localized()
        })
        displayNameLbl = getTextField(with: "displayname".localized())
//        address = getTextField(with: "adress".localized())
//        dobLbl = getTextField(with: "dob".localized())
        configPreviousPageBtn()
        configFinishBtn()
        
        holderView.then({
            $0.addSubview(titleLabel)
            $0.addSubview(displayNameLbl)
            $0.addSubview(carButton)
            $0.addSubview(bikeButton)
            $0.addSubview(previousBtn)
            $0.addSubview(finishBtn)
            $0.addSubview(checkedBox)
            $0.addSubview(termLbl)
        })
        
        titleLabel.snp.makeConstraints({
            $0.top.equalToSuperview().inset(topPadding)
            $0.left.right.equalToSuperview().inset(topPadding)
        })
        
        self.displayNameLbl.snp.makeConstraints({
            $0.top.equalTo(titleLabel.snp.bottom).offset(topPadding)
            $0.centerX.equalToSuperview()
            $0.width.equalToSuperview().inset(topPadding)
        })
        
        if self.signupUserData.userType == .driver {
        bikeButton.snp.makeConstraints({
            $0.top.equalTo(displayNameLbl.snp.bottom).offset(topPadding)
            $0.left.equalToSuperview().inset(topPadding)
//            $0.height.equalTo(buttonHeight)
        })
        
        carButton.snp.makeConstraints({
            $0.width.equalTo(bikeButton.snp.width)
            $0.left.equalTo(bikeButton.snp.right).offset(topPadding)
            $0.bottom.top.equalTo(bikeButton)
            $0.right.equalToSuperview().inset(topPadding)
//            $0.height.equalTo(buttonHeight)
        })
            termLbl.snp.makeConstraints({
                $0.left.equalTo(checkedBox.snp.right)
                $0.top.equalTo(bikeButton.snp.bottom).offset(topPadding)
                $0.right.equalToSuperview().inset(topPadding)
            })
        
      
        } else {
            termLbl.snp.makeConstraints({
                $0.left.equalTo(checkedBox.snp.right)
                $0.top.equalTo(displayNameLbl.snp.bottom).offset(topPadding)
                $0.right.equalToSuperview().inset(topPadding)
            })
        }
        self.checkedBox.snp.makeConstraints({
            $0.left.equalToSuperview().inset(topPadding)
            $0.centerY.equalTo(termLbl.snp.centerY)
            $0.width.height.equalTo(35)
        })
       
        
        self.previousBtn.snp.makeConstraints({
            $0.left.equalToSuperview().inset(topPadding)
            $0.top.equalTo(termLbl.snp.bottom).offset(topPadding)
            $0.bottom.equalToSuperview().inset(topPadding)
        })
        
        self.finishBtn.snp.makeConstraints({
            $0.left.equalTo(previousBtn.snp.right).offset(topPadding)
            $0.width.equalTo(previousBtn.snp.width)
            $0.right.equalToSuperview().inset(topPadding)
            $0.top.bottom.equalTo(previousBtn)
        })
        self.view.layoutIfNeeded()
        
    }
    
    func configPreviousPageBtn() {
        previousBtn.then({
            $0.setTitle("previous".localized(), for: .normal)
            $0.setTitleFont(UIFont.systemFont(ofSize: 15, weight: .semibold), for: .normal)
            $0.setBackgroundColor(.primaryColor(), for: .normal)
            $0.addTarget(self, action: #selector(previousSelected), for: .touchUpInside)
        })
    }
    
    override func updateTextFieldLanguage() {
        previousBtn.setTitle("previous".localized(), for: .normal)
        finishBtn.setTitle("finish".localized(), for: .normal)
        displayNameLbl.placeholder = "displayname".localized()
//        address.placeholder =  "adress".localized()
//        dobLbl.placeholder =  "dob".localized()
    }
    
    func configFinishBtn() {
        finishBtn.then({
            $0.setTitle("finish".localized(), for: .normal)
            $0.setTitleFont(UIFont.systemFont(ofSize: 15, weight: .semibold), for: .normal)
            $0.setBackgroundColor(.primaryColor(), for: .normal)
            $0.addTarget(self, action: #selector(finishSelected), for: .touchUpInside)
        })
    }
    
    @objc func checkedBoxSelected() {
        self.checkedBox.isSelected = !self.checkedBox.isSelected
    }
    
    @objc
    func previousSelected() {
        self.backTapped()
    }
    
    private func showSuccessDialog()  {
        let alertController = MDCAlertController(title: "success".localized(), message: "Your_account_have_been_created_succesfully\nPress_signin_to_conitnue".localized())
        let action = MDCAlertAction(title:"signin".localized().uppercased()) { _ in self.backToHomeScreen() }
        alertController.titleFont = UIFont.systemFont(ofSize: 20, weight: .medium)
        alertController.buttonTitleColor = .primaryColor()
        alertController.addAction(action)
        alertController.cornerRadius = 8
        present(alertController, animated:true, completion: {})
    }
    
//    func {        var ref: DatabaseReference!
//
//    ref = Database.database().reference()
//    let userID = Auth.auth().currentUser?.uid
//    ref.child("users").child(userID!).setValue(["username": "thiennguyen"])
////        ref.child("users").child(userID!).observeSingleEvent(of: .value, with: { (snapshot) in
////          // Get user value
////          let value = snapshot.value as? NSDictionary
////          let username = value?["username"] as? String ?? ""
////          let user = User(username: username)
////
////          // ...
////          }) { (error) in
////            print(error.localizedDescription)
////        }
//    }
    private func showFailDialog() {
        let alertController = MDCAlertController(title: "fail".localized(), message: "please_fill_all_these_information".localized())
        let action = MDCAlertAction(title: "back".localized().uppercased()) { _ in
                alertController.dismiss(animated: true, completion: {})
        }
        alertController.titleFont = UIFont.systemFont(ofSize: 20, weight: .medium)
        alertController.buttonTitleColor = .primaryColor()
        alertController.addAction(action)
        alertController.cornerRadius = 8
        present(alertController, animated:true, completion: {})
    }
    @objc
    func finishSelected() {
        if !self.checkedBox.isSelected {
            let dialog = createPopupDialog(title: "fail".localized(), msg: "please_agree_wtih_our_term_of_service".localized())
            let action = MDCAlertAction(title: "back".localized().uppercased()) { _ in
                dialog.dismiss(animated: true, completion: {})
            }
            dialog.addAction(action)
            present(dialog, animated:true, completion: {})
            return
        }
        if self.displayNameLbl.text?.isEmpty ?? true {
            let dialog = createPopupDialog(title: "fail".localized(), msg: "please_fill_all_these_information".localized())
            let action = MDCAlertAction(title: "back".localized().uppercased()) { _ in
                dialog.dismiss(animated: true, completion: {})
            }
            dialog.addAction(action)
            present(dialog, animated:true, completion: {})
            return
        }
        signupUserData.displayName = self.displayNameLbl.text ?? ""
//        signupUserData.address = self.address.text ?? ""
//        signupUserData.dob = self.dobLbl.text ?? ""
//        signupUserData.checkExistedUser(execute: { isHasUser in
//            print("Thien \(isHasUser)" )
//        })
        signupUserData.vehicleType = self.bikeButton.isSelected ? "bike" : "car"
        signupUserData.postSignupUserData()
        showSuccessDialog()
        
    }
    
    private func backToHomeScreen() {
        let vc = LoginViewController()
        vc.modalPresentationStyle = .fullScreen
        self.performPresentAnimation()
        self.present(vc, animated: false, completion: {self.removeAllLayerAnimation()} )
    }
    

    
    
}

extension NextSignUpViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.frame.origin.y = 0
        })
        view.endEditing(true)
        return true
    }
    
    var floatHeight: CGFloat { return -61}
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        UIView.animate(withDuration: 0.25, animations: {
            //            if textField.isEqual(self.passwordLbl) {
            //                self.view.frame.origin.y = self.floatHeight
            //            } else if textField.isEqual(self.retypePasswordLbl) {
            //                self.view.frame.origin.y = self.floatHeight * 2
            //            } else if textField.isEqual(self.phoneLbl) {
            //                self.view.frame.origin.y = self.floatHeight * 3
            //            } else if textField.isEqual(self.gmailLbl) {
            //                self.view.frame.origin.y = self.floatHeight * 4
            //            } else {
            //                self.view.frame.origin.y = 0
            //            }
        })
        
    }
}
