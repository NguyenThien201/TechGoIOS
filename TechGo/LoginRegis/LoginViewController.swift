//
//  LoginViewController.swift
//  TechGo
//
//  Created by FRIDAY on 23/11/2020.
//

import Foundation
import SnapKit
import UIKit
import MaterialComponents
import Then
import NVActivityIndicatorView
import Firebase
//import GoogleSignIn
import FirebaseAuth

class LoginViewController: CustomViewcontroller, LoginUIProtocol, DialogUIProtocol {
    
    lazy var userNameLbl: MDCOutlinedTextField = getTextField(with: "username".localized())
    lazy var passwordLbl: MDCOutlinedTextField = getTextField(with: "password".localized())
        .then({
            $0.trailingViewMode = .always
            $0.isSecureTextEntry = true
        })
    lazy var titleLabel: UILabel = configTitleLabel("login".localized())
    lazy var signinButton: MDCButton = MDCButton().then({
        $0.setTitle("login".localized(), for: .normal)
        $0.backgroundColor = .primaryColor()
        $0.addTarget(self, action: #selector(signin), for: .touchUpInside)
    })
    
    var visibleButton: UIButton? = nil
    var passwordText: String = ""
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func updateTextFieldLanguage() {
        titleLabel.text = "login".localized()
        userNameLbl.placeholder = "username".localized()
        passwordLbl.placeholder = "password".localized()
        signinButton.setTitle("login".localized(), for: .normal)
        forgotPasswordLbl.setTitle("forgot_password".localized(), for: .normal)
            signupLbl.setTitle("or_create_new_account".localized(), for: .normal)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addLanguageButton()
        self.view.backgroundColor = .lighterPrimaryColor()
        
      
        visibleButton = self.configVisibleButton()
        visibleButton!.addTarget(self, action: #selector(self.visibleDidClicked), for: .touchUpInside)
        
        self.passwordLbl.trailingView = visibleButton
 
        signinButton.setTitleFont(UIFont.systemFont(ofSize: 20, weight: .semibold), for: .normal)
        
        holderView = self.configHolderView()
        
        holderView.then({
            $0.addSubview(titleLabel)
            $0.addSubview(userNameLbl)
            $0.addSubview(passwordLbl)
            $0.addSubview(signinButton)
        })
        
        self.view.addSubview(holderView)
        self.view.addSubview(logoImgView)
        
        logoImgView.snp.makeConstraints({
            $0.bottom.equalTo(holderView.snp.top).offset(-topPadding)
            $0.left.right.equalToSuperview().inset(20)
            $0.height.equalTo(80)
        })
        
        titleLabel.snp.makeConstraints({
            $0.top.equalToSuperview().inset(topPadding)
            $0.left.right.equalToSuperview().inset(20)
        })
        
        self.userNameLbl.snp.makeConstraints({
            $0.top.equalTo(titleLabel.snp.bottom).offset(topPadding)
            $0.centerX.equalToSuperview()
            $0.width.equalToSuperview().inset(20)
        })
        
        self.passwordLbl.snp.makeConstraints({
            $0.width.equalToSuperview().inset(20)
            $0.top.equalTo(userNameLbl.snp.bottom).offset(topPadding)
            $0.centerX.equalToSuperview()
        })
        
        self.signinButton.snp.makeConstraints({
            $0.top.equalTo(passwordLbl.snp.bottom).offset(topPadding)
            $0.left.right.bottom.equalToSuperview().inset(topPadding)
        })
        addLanguageButton()
        addForgotPasswordAndSignupLabel()
    }
    
    let forgotPasswordLbl = UIButton()
    let signupLbl = UIButton()
    
    func addForgotPasswordAndSignupLabel() {
        
        forgotPasswordLbl.then({
            $0.setTitle("forgot_password".localized(), for: .normal)
            $0.setTitleColor(.systemBlue, for: .normal)
            $0.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .medium)
            $0.addTarget(self, action: #selector(presentForgotPasswordVC), for: .touchUpInside)
        })
        
        signupLbl.then({
            $0.setTitle("or_create_new_account".localized(), for: .normal)
            $0.setTitleColor(.systemBlue, for: .normal)
            $0.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .medium)
            $0.addTarget(self, action: #selector(presentCreateAccountVC), for: .touchUpInside)
        })
        
        self.view.addSubview(forgotPasswordLbl)
        self.view.addSubview(signupLbl)
        forgotPasswordLbl.snp.makeConstraints({
            $0.centerX.equalToSuperview()
        })
        signupLbl.snp.makeConstraints({
            $0.top.equalTo(forgotPasswordLbl.snp.bottom).offset(0)
            $0.centerX.equalToSuperview()
            $0.bottom.equalToSuperview().inset(15)
        })
    }

    @objc
    func presentCreateAccountVC() {
        let createNewVc = SignUpViewController()
        createNewVc.modalPresentationStyle = .fullScreen
        self.performPresentAnimation()
        self.present(createNewVc,
                     animated: false,
                     completion: {self.removeAllLayerAnimation()})
    }
    
    @objc
    func presentForgotPasswordVC() {
        let vc = ForgotPasswordViewController()
        vc.modalPresentationStyle = .fullScreen
        self.performPresentAnimation()
        self.present(vc,
                     animated: false,
                     completion: {self.removeAllLayerAnimation()})
    }
    
    @objc
    func visibleDidClicked() {
        reInsertPasswordText()
        self.visibleButton?.isSelected = !self.visibleButton!.isSelected
        self.passwordLbl.isSecureTextEntry = self.visibleButton!.isSelected
    }
    
    @objc
    func signin() {
        view.endEditing(true)
        let animateView = UIView().then({
            $0.backgroundColor = .black
            $0.alpha = 0.4
        })
        self.view.addSubview(animateView)
        animateView.snp.makeConstraints({
            $0.edges.equalToSuperview()
        })
        
        
        let animate = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 80, height: 80), type: .ballRotateChase, color: .primaryColor(), padding: 5)
        animateView.addSubview(animate)
        animate.snp.makeConstraints({$0.center.equalToSuperview()})
        animate.startAnimating()
        
      
        Auth.auth().signIn(withEmail: self.userNameLbl.text! , password: self.passwordLbl.text!) { [weak self] authResult, error in
          guard let strongSelf = self else { return }
            if let err = error {
                animate.stopAnimating()
                animateView.removeFromSuperview()
                let popup = strongSelf.createPopupDialog(title: "login_failed".localized(), msg: "can_not_login_with_these_information\nplease_try_again".localized())
                let action = MDCAlertAction(title: "Try again") { _ in popup.dismiss(animated: true, completion:nil)}
                popup.addAction(action)
                strongSelf.present(popup, animated: true, completion: {})
                print(err)
                return
            }
            else {
//                UserData.shared.fetchingData()
                DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                    animate.stopAnimating()
                    animateView.removeFromSuperview()
                    strongSelf.configTabbarcontroller()
                })
            }
          // ...
        }
    }
    
    func configTabbarcontroller() {
        let homeVC = HomeMenuViewController()
        homeVC.tabBarItem = LoginViewController.getNavBarItem(title: "home".localized(), imgName: "outline_home_white_36pt")
        let activityVC = ActivityViewController()
        activityVC.tabBarItem = LoginViewController.getNavBarItem(title: "activity".localized(), imgName: "outline_event_note_white_36pt")
        let paymentVC = PaymentHistoryViewcontroller()
        paymentVC.tabBarItem = LoginViewController.getNavBarItem(title: "payment".localized(), imgName: "outline_account_balance_wallet_white_36pt")
        let accountVC = AccountSettingViewController()
        accountVC.tabBarItem = LoginViewController.getNavBarItem(title: "account".localized(), imgName: "outline_account_circle_white_36pt")
//        accountVC.tabBarItem.badgeValue = "88"
        let tabBarVC = UITabBarController().then({
            $0.setViewControllers([homeVC, activityVC, paymentVC, accountVC], animated: false)
            $0.modalPresentationStyle = .fullScreen
            $0.tabBar.barTintColor = .darkerPrimaryColor()
            $0.tabBar.tintColor = .white
            $0.tabBar.unselectedItemTintColor = .lighterPrimaryColor()
        })
        tabBarVC.modalTransitionStyle = .crossDissolve
        present(tabBarVC, animated: true, completion: {})

    }
    
    static func getNavBarItem(title: String, imgName: String) -> UITabBarItem {
        let item = UITabBarItem(
            title: title,
            image: UIImage(named: imgName),
            tag: 0)
        item.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -3)
        
        return item
    }

    
    func reInsertPasswordText() {
        let cache = self.passwordLbl.text
        self.passwordLbl.text = cache
        passwordLbl.text?.removeAll()
        passwordLbl.insertText(cache!)
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.isEqual(self.passwordLbl) {
            reInsertPasswordText()
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.isEqual(self.passwordLbl) {
            reInsertPasswordText()
        }
    }
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        guard let textFieldText = textField.text,
//              let rangeOfTextToReplace = Range(range, in: textFieldText) else {
//            return false
//        }
//        let substringToReplace = textFieldText[rangeOfTextToReplace]
//        let count = textFieldText.count - substringToReplace.count + string.count
//
//        return count <= 20
//    }
    
}

