//
//  ResetPasswordViewController.swift
//  TechGo
//
//  Created by Thiện Nguyễn on 20/12/2020.
//
import Foundation
import SnapKit
import MaterialComponents
import Then
import FirebaseAuth

enum dialogCase {
    case empty, notmatch, home
}

class ResetPasswordViewController: CustomViewcontroller, LoginUIProtocol {
    
    var nextVC = LoginViewController().then({
        $0.modalPresentationStyle = .fullScreen
    })
    
    
    var visibleButton: UIButton? = nil
    var retypeVisibleButton: UIButton? = nil
    var oldVisibleButton: UIButton? = nil
    
    var nextPageBtn = MDCButton()
    var oldPasswordLbl: MDCOutlinedTextField!
    var passwordLbl: MDCOutlinedTextField!
    var retypePasswordLbl: MDCOutlinedTextField!
    var isBackToSignIn: Bool = true
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addBackButton()
        self.addLanguageButton()
        self.view.backgroundColor = .lighterPrimaryColor()
        holderView = self.configHolderView()
        
        self.addLogoImg()
        let titleLabel = self.configTitleLabel("reset_password".localized())
        oldPasswordLbl = getTextField(with: "oldpassword".localized()).then({
            $0.trailingViewMode = .always
            $0.isSecureTextEntry = true
        })
        passwordLbl = getTextField(with: "password".localized()).then({
            $0.trailingViewMode = .always
            $0.isSecureTextEntry = true
        })
        
        retypePasswordLbl = getTextField(with: "retype_password".localized()).then({
            $0.trailingViewMode = .always
            $0.isSecureTextEntry = true
        })
        oldPasswordLbl.delegate = self
        passwordLbl.delegate = self
        retypePasswordLbl.delegate = self
        let newLbl = UILabel().then({
            $0.numberOfLines = -1
            $0.text = "input_new_pass".localized()
            $0.font =  UIFont.systemFont(ofSize: 18, weight: .regular)
        })
        
        holderView.addSubview(titleLabel)
        configNextPageBtn()
        
        holderView.then({
            $0.addSubview(newLbl)
            $0.addSubview(oldPasswordLbl)
            $0.addSubview(passwordLbl)
            $0.addSubview(retypePasswordLbl)
            $0.addSubview(nextPageBtn)
        })
        
        titleLabel.snp.makeConstraints({
            $0.top.equalToSuperview().inset(topPadding)
            $0.left.right.equalToSuperview().inset(20)
        })
        
        newLbl.snp.makeConstraints({
            $0.left.right.equalToSuperview().inset(topPadding)
            $0.top.equalTo(titleLabel.snp.bottom).offset(topPadding)
        })
        
        visibleButton = self.configVisibleButton()
        visibleButton!.addTarget(self, action: #selector(self.visibleDidClicked), for: .touchUpInside)
        
        retypeVisibleButton = self.configVisibleButton()
        retypeVisibleButton!.addTarget(self, action: #selector(self.retypeVisibleDidClicked), for: .touchUpInside)
        
        oldVisibleButton = self.configVisibleButton()
        oldVisibleButton!.addTarget(self, action: #selector(self.oldVisibleDidClicked), for: .touchUpInside)
        
        self.oldPasswordLbl.trailingView = oldVisibleButton
        self.passwordLbl.trailingView = visibleButton
        self.retypePasswordLbl.trailingView = retypeVisibleButton
        
        oldPasswordLbl.snp.makeConstraints({
            $0.width.equalToSuperview().inset(20)
            $0.top.equalTo(newLbl.snp.bottom).offset(topPadding)
            $0.centerX.equalToSuperview()
        })
        self.passwordLbl.snp.makeConstraints({
            $0.width.equalToSuperview().inset(20)
            $0.top.equalTo(oldPasswordLbl.snp.bottom).offset(topPadding)
            $0.centerX.equalToSuperview()
        })
        
        self.retypePasswordLbl.snp.makeConstraints({
            $0.width.equalToSuperview().inset(20)
            $0.top.equalTo(passwordLbl.snp.bottom).offset(topPadding)
            $0.centerX.equalToSuperview()
        })
        
        self.nextPageBtn.snp.makeConstraints({
            $0.top.equalTo(retypePasswordLbl.snp.bottom).offset(topPadding)
            $0.right.bottom.equalToSuperview().inset(topPadding)
            $0.left.equalTo(self.holderView.snp.centerX).inset(topPadding/2)
        })
        
        
        
    }
    
    func configNextPageBtn() {
        nextPageBtn.then({
            $0.setTitle("finish".localized(), for: .normal)
            $0.setTitleFont(UIFont.systemFont(ofSize: 15, weight: .semibold), for: .normal)
            $0.setBackgroundColor(.primaryColor(), for: .normal)
            $0.addTarget(self, action: #selector(nextPage), for: .touchUpInside)
        })
    }
    
    @objc
    func nextPage() {
    
        
        if self.passwordLbl.text?.isEmpty ?? true || self.retypePasswordLbl.text?.isEmpty ?? true {
            showFailDialog(.empty)
        } else if self.passwordLbl.text != self.retypePasswordLbl.text {
            showFailDialog(.notmatch)
        } else {
//            let user = FIRAuth.auth()?.currentUser
//            let credential = FIREmailPasswordAuthProvider.credentialWithEmail(email, password: currentPassword)
//
//            user?.reauthenticateWithCredential(credential, completion: { (error) in
//                if error != nil{
//                    self.displayAlertMessage("Error reauthenticating user")
//                }else{
//                    //change to new password
//                }
//            })
        }
        showFailDialog(.home)
        
    }
    
    private func showFailDialog(_ dcase: dialogCase) {
        var title = ""
        var text = ""
        var btn = ""
        switch dcase {
        case .empty:
            title = "fail"
            btn = "back"
            text = "please_fill_all_these_information"
        case .notmatch:
            title = "fail"
            btn = "back"
            text = "password_notMatch"
        default:
            title = "success"
            btn = isBackToSignIn ? "signin" : "finish"
            text = isBackToSignIn ? "Your_password_has_been_reset_succesfully\nPress_signin_to_conitnue" : "Your_password_has_been_reset_succesfully\nPress_finish_to_conitnue"
        }
        
        let alertController = MDCAlertController(title: title.localized(), message: text.localized())
        let action = MDCAlertAction(title: btn.localized().uppercased()) { _ in
            alertController.dismiss(animated: true, completion: {})
            if dcase == .home {
                if self.isBackToSignIn {
                    self.performPresentAnimation()
                    self.present(self.nextVC, animated: false, completion: {
                        self.removeAllLayerAnimation()
                    })
                } else {
                    if btn == "finish" {
                        Auth.auth().currentUser?.updatePassword(to: self.passwordLbl.text ?? "" , completion: {
                            _ in
                        })
                    }
                    self.backTapped()
                }
            }
        }
        alertController.titleFont = UIFont.systemFont(ofSize: 20, weight: .medium)
        alertController.buttonTitleColor = .primaryColor()
        alertController.addAction(action)
        alertController.cornerRadius = 8
        present(alertController, animated:true, completion: {})
    }
    
    @objc
    func visibleDidClicked() {
        reInsertPasswordText(for :self.passwordLbl)
        self.visibleButton?.isSelected = !self.visibleButton!.isSelected
        self.passwordLbl.isSecureTextEntry = self.visibleButton!.isSelected
    }
    
    @objc
    func retypeVisibleDidClicked() {
        reInsertPasswordText(for :self.retypePasswordLbl)
        self.retypeVisibleButton?.isSelected = !self.retypeVisibleButton!.isSelected
        self.retypePasswordLbl.isSecureTextEntry = self.retypeVisibleButton!.isSelected
    }
    
    @objc
    func oldVisibleDidClicked() {
        reInsertPasswordText(for :self.oldPasswordLbl)
        self.oldVisibleButton?.isSelected = !self.oldVisibleButton!.isSelected
        self.oldPasswordLbl.isSecureTextEntry = self.oldVisibleButton!.isSelected
    }
    
    
    func reInsertPasswordText(for tf: MDCOutlinedTextField?) {
        let cache = tf!.text
        tf?.text = cache
        tf?.text?.removeAll()
        tf?.insertText(cache!)
    }
    
}

extension ResetPasswordViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.frame.origin.y = 0
        })
        view.endEditing(true)
        return true
    }

    
    var floatHeight: CGFloat { return -61}
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.reInsertPasswordText(for: textField as? MDCOutlinedTextField)
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.25, animations: {
        })
        
    }
}
