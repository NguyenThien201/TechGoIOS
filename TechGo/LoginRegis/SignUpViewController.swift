//
//  SignUpViewController.swift
//  TechGo
//
//  Created by FRIDAY on 15/12/2020.
//

import Foundation
import SnapKit
import MaterialComponents
import Then


class SignUpViewController: CustomViewcontroller, LoginUIProtocol {
    var signupUserData: UserData = UserData()
    var nextVC = NextSignUpViewController().then({
        $0.modalPresentationStyle = .fullScreen
    })
    
    var customerBtn = MDCButton()
    var driverBtn = MDCButton()
    var visibleButton: UIButton? = nil
    var retypeVisibleButton: UIButton? = nil
    var nextPageBtn = MDCButton()
        
//    var userNameLbl: MDCOutlinedTextField!
    var passwordLbl: MDCOutlinedTextField!
    var retypePasswordLbl: MDCOutlinedTextField!
    var phoneLbl: MDCOutlinedTextField!
    var gmailLbl: MDCOutlinedTextField!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addBackButton()
        self.addLanguageButton()
        self.view.backgroundColor = .lighterPrimaryColor()
        holderView = self.configHolderView()
      
        self.view.addSubview(logoImgView)
        logoImgView.snp.makeConstraints({
            $0.left.right.equalToSuperview().inset(20)
            $0.top.equalToSuperview().inset(20)
            $0.height.equalTo(80)
        })
        let titleLabel = self.configTitleLabel("signup".localized())
        
//        userNameLbl = getTextField(with: "username".localized())
        passwordLbl = getTextField(with: "password".localized()).then({
            $0.trailingViewMode = .always
            $0.isSecureTextEntry = true
        })
        retypePasswordLbl = getTextField(with: "retype_password".localized()).then({
            $0.trailingViewMode = .always
            $0.isSecureTextEntry = true
        })
        phoneLbl = getTextField(with: "phone_number".localized())
        gmailLbl = getTextField(with: "email".localized())
        
        let iAmLbl = UILabel().then({
            $0.text = "iam".localized()
            $0.font =  UIFont.systemFont(ofSize: 18, weight: .regular)
        })
        holderView.addSubview(titleLabel)
        configUserTypeButton()
        configNextPageBtn()
        
        holderView.then({
            $0.addSubview(iAmLbl)
            $0.addSubview(self.customerBtn)
            $0.addSubview(self.driverBtn)
//            $0.addSubview(userNameLbl)
            $0.addSubview(passwordLbl)
            $0.addSubview(retypePasswordLbl)
            $0.addSubview(phoneLbl)
            $0.addSubview(gmailLbl)
            $0.addSubview(nextPageBtn)
        })
        
        titleLabel.snp.makeConstraints({
            $0.top.equalToSuperview().inset(topPadding)
            $0.left.right.equalToSuperview().inset(20)
        })
        
        iAmLbl.snp.makeConstraints({
            $0.left.equalToSuperview().inset(topPadding)
            $0.top.equalTo(titleLabel.snp.bottom).offset(topPadding/2)
        })
        
        customerBtn.snp.makeConstraints({
            $0.top.equalTo(iAmLbl.snp.bottom).offset(topPadding/21)
            $0.left.equalToSuperview().inset(topPadding)
        })
        
        driverBtn.snp.makeConstraints({
            $0.width.equalTo(customerBtn.snp.width)
            $0.left.equalTo(customerBtn.snp.right).offset(topPadding)
            $0.bottom.top.equalTo(customerBtn)
            $0.right.equalToSuperview().inset(topPadding)
        })
        
        visibleButton = self.configVisibleButton()
        visibleButton!.addTarget(self, action: #selector(self.visibleDidClicked), for: .touchUpInside)
        
        retypeVisibleButton = self.configVisibleButton()
        retypeVisibleButton!.addTarget(self, action: #selector(self.retypeVisibleDidClicked), for: .touchUpInside)
        
        self.passwordLbl.trailingView = visibleButton
        self.retypePasswordLbl.trailingView = retypeVisibleButton
        
        self.gmailLbl.snp.makeConstraints({
            $0.top.equalTo(customerBtn.snp.bottom).offset(topPadding)
            $0.centerX.equalToSuperview()
            $0.width.equalToSuperview().inset(20)
        })
        
        self.passwordLbl.snp.makeConstraints({
            $0.width.equalToSuperview().inset(20)
            $0.top.equalTo(gmailLbl.snp.bottom).offset(topPadding)
            $0.centerX.equalToSuperview()
        })
        
        self.retypePasswordLbl.snp.makeConstraints({
            $0.width.equalToSuperview().inset(20)
            $0.top.equalTo(passwordLbl.snp.bottom).offset(topPadding)
            $0.centerX.equalToSuperview()
        })
        
        self.phoneLbl.snp.makeConstraints({
            $0.width.equalToSuperview().inset(20)
            $0.top.equalTo(retypePasswordLbl.snp.bottom).offset(topPadding)
            $0.centerX.equalToSuperview()
        })
        
//        self.gmailLbl.snp.makeConstraints({
//            $0.width.equalToSuperview().inset(20)
//            $0.top.equalTo(phoneLbl.snp.bottom).offset(topPadding)
//            $0.centerX.equalToSuperview()
//
//        })
        
        self.nextPageBtn.snp.makeConstraints({
            $0.top.equalTo(phoneLbl.snp.bottom).offset(topPadding)
            $0.right.bottom.equalToSuperview().inset(topPadding)
            $0.width.equalTo(driverBtn.snp.width)
        })
        
        
        
    }
    
    func configUserTypeButton() {
        self.driverBtn.then({
            $0.setTitle("driver".localized(), for: .normal)
            $0.setTitleFont(UIFont.systemFont(ofSize: 15, weight: .semibold), for: .normal)
            $0.setBackgroundColor(.lighterPrimaryColor(), for: .normal)
            $0.setBackgroundColor(.primaryColor(), for: .selected)
            $0.addTarget(self, action: #selector(diverSelected), for: .touchUpInside)
            
        })
        self.customerBtn.then({
            $0.setTitle("customer".localized(), for: .normal)
            $0.setTitleFont(UIFont.systemFont(ofSize: 15, weight: .semibold), for: .normal)
            $0.setBackgroundColor(.lighterPrimaryColor(), for: .normal)
            $0.setBackgroundColor(.primaryColor(), for: .selected)
            $0.addTarget(self, action: #selector(customerSelected), for: .touchUpInside)
        })
        self.customerSelected()
    }
    
    func configNextPageBtn() {
        nextPageBtn.then({
            $0.setTitle("next".localized(), for: .normal)
            $0.setTitleFont(UIFont.systemFont(ofSize: 15, weight: .semibold), for: .normal)
            $0.setBackgroundColor(.primaryColor(), for: .normal)
            $0.addTarget(self, action: #selector(nextPage), for: .touchUpInside)
        })
    }
    
    @objc
    func nextPage() {

        if self.passwordLbl.text?.isEmpty ?? true || self.retypePasswordLbl.text?.isEmpty ?? true {
            showFailDialog(.empty)
            return
        } else if self.passwordLbl.text != self.retypePasswordLbl.text {
            showFailDialog(.notmatch)
            return
        }
        self.updateSignupData()
        
        self.performPresentAnimation()
        
        nextVC.signupUserData = signupUserData
        self.present(nextVC, animated: false, completion: {
            self.removeAllLayerAnimation()
        })
    }
    
    func updateSignupData() {
//        self.signupUserData.userName = self.userNameLbl.text ?? ""
        self.signupUserData.password = self.passwordLbl.text ?? ""
        self.signupUserData.phoneNumber = self.phoneLbl.text ?? ""
        self.signupUserData.email = self.gmailLbl.text ?? ""
    }
    
    private func showFailDialog(_ dcase: dialogCase) {
        var title = ""
        var text = ""
        var btn = ""
        switch dcase {
        case .empty:
            title = "fail"
            btn = "back"
            text = "please_fill_all_these_information"
        case .notmatch:
            title = "fail"
            btn = "back"
            text = "password_notMatch"
        default:
            return
        }
        let alertController = MDCAlertController(title: title.localized(), message: text.localized())
        let action = MDCAlertAction(title: btn.localized().uppercased()) { _ in
                alertController.dismiss(animated: false, completion: {})
        }
        alertController.titleFont = UIFont.systemFont(ofSize: 20, weight: .medium)
        alertController.buttonTitleColor = .primaryColor()
        alertController.addAction(action)
        alertController.cornerRadius = 8
        present(alertController, animated:true, completion: {})
    }
    
    @objc
    func diverSelected() {
        self.customerBtn.isSelected = false
        self.driverBtn.isSelected = true
        self.signupUserData.userType = UserType.driver
    }
    
    @objc
    func customerSelected() {
        self.driverBtn.isSelected = false
        self.customerBtn.isSelected = true
        self.signupUserData.userType = UserType.customer
    }
    
    @objc
    func visibleDidClicked() {
        reInsertPasswordText(for :self.passwordLbl)
        self.visibleButton?.isSelected = !self.visibleButton!.isSelected
        self.passwordLbl.isSecureTextEntry = self.visibleButton!.isSelected
    }
    
    @objc
    func retypeVisibleDidClicked() {
        reInsertPasswordText(for :self.retypePasswordLbl)
        self.retypeVisibleButton?.isSelected = !self.visibleButton!.isSelected
        self.retypePasswordLbl.isSecureTextEntry = self.visibleButton!.isSelected
    }
    
    func reInsertPasswordText(for tf: MDCOutlinedTextField?) {
        let cache = tf!.text
        tf?.text = cache
        tf?.text?.removeAll()
        tf?.insertText(cache!)
    }

    
}

extension SignUpViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.frame.origin.y = 0
        })
        view.endEditing(true)
        return true
    }
    
    var floatHeight: CGFloat { return -61}
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        UIView.animate(withDuration: 0.25, animations: {
            if textField.isEqual(self.passwordLbl) {
                self.view.frame.origin.y = self.floatHeight
            } else if textField.isEqual(self.retypePasswordLbl) {
                self.view.frame.origin.y = self.floatHeight * 2
            } else if textField.isEqual(self.phoneLbl) {
                self.view.frame.origin.y = self.floatHeight * 3
            } else {
                self.view.frame.origin.y = 0
            }
        })
        
    }
}
