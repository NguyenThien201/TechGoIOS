//
//  DialogUIProtocol.swift
//  TechGo
//
//  Created by Thiện Nguyễn on 22/12/2020.
//

import Foundation
import UIKit
import MaterialComponents

protocol DialogUIProtocol {
}
extension DialogUIProtocol where Self: UIViewController {
    func createPopupDialog(title: String, msg: String) -> MDCAlertController {
        let alertController = MDCAlertController(title: title, message: msg)
        alertController.titleFont = UIFont.systemFont(ofSize: 20, weight: .medium)
        alertController.buttonTitleColor = .primaryColor()
        alertController.cornerRadius = 8
        return alertController
    }
}
