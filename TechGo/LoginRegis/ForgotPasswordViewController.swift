//
//  ForgotPasswordViewController.swift
//  TechGo
//
//  Created by Thiện Nguyễn on 20/12/2020.
//

import Foundation
import SnapKit
import MaterialComponents
import Then
import Firebase
import FirebaseAuth
import FirebaseDatabase

class ForgotPasswordViewController: CustomViewcontroller, LoginUIProtocol {
    
    var nextVC = LoginViewController().then({
        $0.modalPresentationStyle = .fullScreen
    })

    var nextPageBtn = MDCButton()
        
    var retypeEmailLbl: MDCOutlinedTextField!
    var gmailLbl: MDCOutlinedTextField!
    var inputLbl = UILabel().then({$0.text = "input_these_info_to".localized()})
    lazy var titleLabel = configTitleLabel("forgot_password".localized())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addBackButton()
        self.addLanguageButton()
        self.view.backgroundColor = .lighterPrimaryColor()
        holderView = self.configHolderView()
      
		self.addLogoImg()
        
        
        retypeEmailLbl = getTextField(with: "retype_email".localized())
        gmailLbl = getTextField(with: "email".localized())
        
        inputLbl.then({
            $0.numberOfLines = -1
            $0.font =  UIFont.systemFont(ofSize: 18, weight: .regular)
        })
        holderView.addSubview(titleLabel)
        configNextPageBtn()
        
        holderView.then({
            $0.addSubview(inputLbl)
            $0.addSubview(retypeEmailLbl)
            $0.addSubview(gmailLbl)
            $0.addSubview(nextPageBtn)
        })
        
        titleLabel.snp.makeConstraints({
            $0.top.equalToSuperview().inset(topPadding)
            $0.left.right.equalToSuperview().inset(20)
        })
        
        inputLbl.snp.makeConstraints({
            $0.left.right.equalToSuperview().inset(topPadding)
            $0.top.equalTo(titleLabel.snp.bottom).offset(topPadding)
        })
  
        self.gmailLbl.snp.makeConstraints({
            $0.top.equalTo(inputLbl.snp.bottom).offset(topPadding)
            $0.centerX.equalToSuperview()
            $0.width.equalToSuperview().inset(20)
        })

        
        self.retypeEmailLbl.snp.makeConstraints({
            $0.width.equalToSuperview().inset(20)
            $0.top.equalTo(gmailLbl.snp.bottom).offset(topPadding)
            $0.centerX.equalToSuperview()
            
        })
        
        self.nextPageBtn.snp.makeConstraints({
            $0.top.equalTo(retypeEmailLbl.snp.bottom).offset(topPadding)
            $0.right.bottom.equalToSuperview().inset(topPadding)
            $0.left.equalTo(self.holderView.snp.centerX).inset(topPadding/2)
        })
                        
    }
    
    func configNextPageBtn() {
        nextPageBtn.then({
            $0.setTitle("next".localized(), for: .normal)
            $0.setTitleFont(UIFont.systemFont(ofSize: 15, weight: .semibold), for: .normal)
            $0.setBackgroundColor(.primaryColor(), for: .normal)
            $0.addTarget(self, action: #selector(nextPage), for: .touchUpInside)
        })
    }
    
    @objc
    func nextPage() {
//        Auth.auth().sendPasswordReset(withEmail: self.) { (error) in
//                              print("email err \(error)"  )
//                            }
//        let ref = Database.database().reference()
//        if let user = Auth.auth().currentUser {
//            ref.child("users").observeSingleEvent(of: .value, with: {(snaps) in
//                guard let value = snaps.value as? NSDictionary else {
//                    return
//                }
//                let account = value.filter { (key, value) -> Bool in
//                    print(key)
//                    print(value)
//                    return true
//                }
//
//                let account1 = value.filter { item in
//                    if let itemList = item.value as? NSDictionary,
//                       let email = itemList["email"] as? String,
//                       email == self.gmailLbl.text {
//                        return true
//                    }
//                    return false
//                }
//                if account1.count > 0,
//                   let mail = account1.first as? String {
//                    Auth.auth().sendPasswordReset(withEmail: mail) { (error) in
//                      print("email err \(error)"  )
//                    }
//                }
//            })
        
            showDialog()
    }
    
    private func showDialog() {
        var title = "fail"
        var msg = "email_not_match"
        var actionMsg = "back"
        if !(self.gmailLbl.text?.isEmpty ?? true), self.gmailLbl.text == self.retypeEmailLbl.text {
            Auth.auth().sendPasswordReset(withEmail: self.gmailLbl.text ?? "") { (error) in
           //                      print("email err \(error)"  )
                               }
            title = "success"
            msg = "we_have_send_a_email_to_your_mail_adress_if_the_email_match_any_account,_please_check_your_mail_box_to_reset_password"
            actionMsg = "finish"
            
        }
        let alertController = MDCAlertController(title: title.localized(), message: msg.localized())
        let action = MDCAlertAction(title:actionMsg.localized().uppercased()) { _ in
            alertController.dismiss(animated: true, completion: {
                if actionMsg == "finish" {
                    self.performPresentAnimation()
                    self.present(self.nextVC, animated: false, completion: {
                        self.removeAllLayerAnimation()
                    })
                }
            })
                    
        }
        alertController.titleFont = UIFont.systemFont(ofSize: 20, weight: .medium)
        alertController.buttonTitleColor = .primaryColor()
        alertController.addAction(action)
        alertController.cornerRadius = 8
        present(alertController, animated:true, completion: {})
    }
    override func updateTextFieldLanguage() {
        self.titleLabel.text = "forgot_password".localized()
        self.gmailLbl.placeholder = "email".localized()
        self.retypeEmailLbl.placeholder = "retype_email".localized()
        inputLbl.text = "input_these_info_to".localized()
        nextPageBtn.setTitle("next".localized(), for: .normal)
        self.backButton.setTitle("back".localized(), for: .normal)
    }
}

extension ForgotPasswordViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.frame.origin.y = 0
        })
        view.endEditing(true)
        return true
    }
    
    var floatHeight: CGFloat { return -61}
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        UIView.animate(withDuration: 0.25, animations: {
//            if textField.isEqual(self.passwordLbl) {
//                self.view.frame.origin.y = self.floatHeight
//            } else if textField.isEqual(self.retypePasswordLbl) {
//                self.view.frame.origin.y = self.floatHeight * 2
//            } else if textField.isEqual(self.phoneLbl) {
//                self.view.frame.origin.y = self.floatHeight * 3
//            } else if textField.isEqual(self.gmailLbl) {
//                self.view.frame.origin.y = self.floatHeight * 4
//            } else {
//                self.view.frame.origin.y = 0
//            }
        })
        
    }
}
