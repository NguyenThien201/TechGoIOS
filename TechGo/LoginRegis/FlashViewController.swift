//
//  FlashViewController.swift
//  TechGo
//
//  Created by Thiện Nguyễn on 03/01/2021.
//


import Foundation
import SnapKit
import UIKit
import MaterialComponents
import Then
import NVActivityIndicatorView
import Firebase
//import GoogleSignIn
import FirebaseAuth
import CoreLocation


class FlashViewController: UIViewController, CLLocationManagerDelegate {
    var currentLocation: CLLocation! {
        didSet {
            if let currentLocation = currentLocation {
                UserData.shared.location = currentLocation.coordinate
                UserData.shared.updateLocation()
                print(currentLocation.coordinate)
            }
            
        }
    }
    var isShow = true
    var locationManager = CLLocationManager()
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("error::: \(error)")
        locationManager.stopUpdatingLocation()
        let alert = UIAlertController(title: "Settings", message: "Allow location from settings", preferredStyle: UIAlertController.Style.alert)
        self.present(alert, animated: true, completion: nil)
        alert.addAction(UIAlertAction(title: "gps", style: .default, handler: { action in
            switch action.style{
            case .default: UIApplication.shared.openURL( URL(string: UIApplication.openSettingsURLString)!)
            case .cancel: print("cancel")
            case .destructive: print("destructive")
            }
        }))
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        var locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways {
            if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self) {
                if CLLocationManager.isRangingAvailable() {
                    // do stuff
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.locationManager.requestAlwaysAuthorization()

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        if
           CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
               CLLocationManager.authorizationStatus() ==  .authorizedAlways
       {
           self.currentLocation = self.locationManager.location
       }
        let timer = Timer.scheduledTimer(withTimeInterval: 20, repeats: true, block: {
            _ in
             if
                CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
                    CLLocationManager.authorizationStatus() ==  .authorizedAlways
            {
                self.currentLocation = self.locationManager.location
            }
        })
        
//        UserData.shared.fetchingData()
        self.view.backgroundColor = .lighterPrimaryColor()
        let logo = UIImageView(image: UIImage(named: "logoIcon"))
        logo.contentMode = .scaleAspectFit
        self.view.addSubview(logo)
        logo.snp.makeConstraints({
            $0.center.equalToSuperview()
            $0.height.equalTo(100)
        })
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if isShow {
            show()
        }
    }
    func show() {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute: {
                    
                    if let _ = Auth.auth().currentUser {
                        self.configTabbarcontroller()
                        return
                    } else {
                        let vc = LoginViewController()
                        vc.modalTransitionStyle = .crossDissolve
                        vc.modalPresentationStyle = .fullScreen
                        self.present(vc, animated: true, completion: {})
                    }
                })

    }
    
    func configTabbarcontroller() {
        let homeVC = HomeMenuViewController()
        homeVC.tabBarItem = FlashViewController.getNavBarItem(title: "home".localized(), imgName: "outline_home_white_36pt")
        let activityVC = ActivityViewController()
        activityVC.tabBarItem = FlashViewController.getNavBarItem(title: "activity".localized(), imgName: "outline_event_note_white_36pt")
        let paymentVC = PaymentHistoryViewcontroller()
        paymentVC.tabBarItem = FlashViewController.getNavBarItem(title: "payment".localized(), imgName: "outline_account_balance_wallet_white_36pt")
        let accountVC = AccountSettingViewController()
        accountVC.tabBarItem = FlashViewController.getNavBarItem(title: "account".localized(), imgName: "outline_account_circle_white_36pt")
        let tabBarVC = UITabBarController().then({
            $0.setViewControllers([homeVC, activityVC, paymentVC, accountVC], animated: false)
            $0.modalPresentationStyle = .fullScreen
            $0.tabBar.barTintColor = .darkerPrimaryColor()
            $0.tabBar.tintColor = .white
            $0.tabBar.unselectedItemTintColor = .lighterPrimaryColor()
        })
        tabBarVC.selectedIndex = 0
        tabBarVC.modalTransitionStyle = .crossDissolve
        present(tabBarVC, animated: true, completion: {})

    }
    static public func getTabbarcontroller() -> UITabBarController {
        let homeVC = HomeMenuViewController()
        homeVC.tabBarItem = FlashViewController.getNavBarItem(title: "home".localized(), imgName: "outline_home_white_36pt")
        let activityVC = ActivityViewController()
        activityVC.tabBarItem = FlashViewController.getNavBarItem(title: "activity".localized(), imgName: "outline_event_note_white_36pt")
        let paymentVC = PaymentHistoryViewcontroller()
        paymentVC.tabBarItem = FlashViewController.getNavBarItem(title: "payment".localized(), imgName: "outline_account_balance_wallet_white_36pt")
        let accountVC = AccountSettingViewController()
        accountVC.tabBarItem = FlashViewController.getNavBarItem(title: "account".localized(), imgName: "outline_account_circle_white_36pt")
//        accountVC.tabBarItem.badgeValue = "88"
        let tabBarVC = UITabBarController().then({
            $0.setViewControllers([homeVC, activityVC, paymentVC, accountVC], animated: false)
            $0.modalPresentationStyle = .fullScreen
            $0.tabBar.barTintColor = .darkerPrimaryColor()
            $0.tabBar.tintColor = .white
            $0.tabBar.unselectedItemTintColor = .lighterPrimaryColor()
        })
        tabBarVC.modalTransitionStyle = .crossDissolve
        return tabBarVC
    }
    
    static func getNavBarItem(title: String, imgName: String) -> UITabBarItem {
        let item = UITabBarItem(
            title: title,
            image: UIImage(named: imgName),
            tag: 0)
        item.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -3)
        
        return item
    }
}
