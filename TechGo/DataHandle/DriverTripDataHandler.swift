//
//  DriverTripDataHandler.swift
//  TechGo
//
//  Created by Thiện Nguyễn on 06/01/2021.
//

import Foundation
import LocationPicker
import Firebase
import FirebaseAuth
import FirebaseDatabase
import CoreLocation
import MapKit

class DriverTripDataHandler {
    static var shared: DriverTripDataHandler = DriverTripDataHandler()
    
    var fromLocation: Location? = nil
    var toLocation: Location? = nil
    
    var driverLocation: Location? = nil
    var driverUid: String = ""
    
    var totalCost: Int = 0
    
    var vehicleType: VehicleType = .bike
    var paymentMethod: TripPaymentType = .cash
    var isActive: Bool = true
    var userData = UserData()
    
    func beginCheckTrip(action: @escaping () -> Void ) {
        var time = Timer()
        func block() {
            if self.isActive {
                if let user = Auth.auth().currentUser {
                    let ref = Database.database().reference()
                    ref.child("users").child(user.uid).observeSingleEvent(of: .value, with: {(snaps) in
                        guard let value = snaps.value as? NSDictionary else {
                            return
                        }
                        if let active = value["isActive"] as? Int,
                           active == 0 {
                            self.isActive = false
                            ref.child("users").child(user.uid).child("trips").observeSingleEvent(of: .value, with: {(snaps) in
                                guard let value = snaps.value as? NSDictionary else {
                                    return
                                }
                                let last = value.count - 1
                                ref.child("users").child(user.uid).child("trips").child("trip\(last)").observeSingleEvent(of: .value, with: {(snaps) in
                                    guard let value = snaps.value as? NSDictionary else {
                                        return
                                    }
                                    if let fromLocList = value["from"] as? NSDictionary,
                                       let fromLat = fromLocList["lat"] as? Double,
                                       let fromLng = fromLocList["lng"] as? Double {
                                        
                                        self.fromLocation = Location.init(name: nil, location: CLLocation(latitude: fromLat, longitude: fromLng), placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: fromLat, longitude: fromLng), addressDictionary: nil))
                                    }
                                    
                                    if let toLocList = value["to"] as? NSDictionary,
                                       let lat = toLocList["lat"] as? Double,
                                       let lng = toLocList["lng"] as? Double {
                                        
                                        self.toLocation = Location.init(name: nil, location: CLLocation(latitude: lat, longitude: lng), placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: lat, longitude: lng), addressDictionary: nil))
                                    }
                                    self.totalCost = value["cost"] as? Int ?? 0
                                    
                                    self.driverUid = value["partnerUid"] as? String ?? ""
                                    self.paymentMethod = TripPaymentType(rawValue: value["paymentMethod"] as? String ?? "cash") ?? .cash
                                    
                                    print("have trip")
                                    self.configCustomerData(action: action)
                                    time.invalidate()
                                    print("have trip")
                                })
                            })
                            
                        }
                        
                    })
                }
            }
        }
         time = Timer.scheduledTimer(withTimeInterval: 2, repeats: true, block: {_ in block()})
        
        
    }
    
    func configCustomerData(action: @escaping () -> Void) {
        let ref = Database.database().reference()
        ref.child("users").child(self.driverUid).observeSingleEvent(of: .value, with: {(snaps) in
            guard let value = snaps.value as? NSDictionary else {
                return
            }
            self.userData.displayName = value["displayName"] as? String ?? ""
            self.userData.phoneNumber = value["phoneNumber"] as? String ?? ""
            action()
            return
        })
//        action()
    }
    
    func cancelTrip() {
        let ref = Database.database().reference()
        if let user = Auth.auth().currentUser {
            ref.child("users").child(user.uid).child("trips").observeSingleEvent(of: .value, with: {(snaps) in
                guard let value = snaps.value as? NSDictionary else {
                    return
                }
                let last = value.count - 1
                ref.child("users").child(user.uid).child("trips").child("trip\(last)").updateChildValues(["driverCancel":true])
                
            })
            ref.child("users").child(driverUid).child("trips").observeSingleEvent(of: .value, with: {(snaps) in
                guard let value = snaps.value as? NSDictionary else {
                    return
                }
                let last = value.count - 1
                ref.child("users").child(self.driverUid).child("trips").child("trip\(last)").updateChildValues(["driverCancel":true])
                
            })
            DispatchQueue.main.asyncAfter(deadline: .now() + 10, execute: {
                if let user = Auth.auth().currentUser {
                    ref.child("users").child(user.uid).updateChildValues(["isActive":true])
                    self.isActive = true
                }
            })
        }
    }
    
    func pickedCustomer() {
        let ref = Database.database().reference()
        if let user = Auth.auth().currentUser {
            ref.child("users").child(user.uid).child("trips").observeSingleEvent(of: .value, with: {(snaps) in
                guard let value = snaps.value as? NSDictionary else {
                    return
                }
                let last = value.count - 1
                ref.child("users").child(user.uid).child("trips").child("trip\(last)").updateChildValues(["picked":true])
            })
            
            ref.child("users").child(driverUid).child("trips").observeSingleEvent(of: .value, with: {(snaps) in
                guard let value = snaps.value as? NSDictionary else {
                    return
                }
                let last = value.count - 1
                ref.child("users").child(self.driverUid).child("trips").child("trip\(last)").updateChildValues(["picked":true])
            })
        }
    }
    
    func completedTrip() {
        let ref = Database.database().reference()
        self.isActive = true
        ref.child("users").child(self.driverUid).updateChildValues(["isActive":true])
        if let user = Auth.auth().currentUser {
            ref.child("users").child(user.uid).updateChildValues(["isActive":true])
            ref.child("users").child(user.uid).child("trips").observeSingleEvent(of: .value, with: {(snaps) in
                guard let value = snaps.value as? NSDictionary else {
                    return
                }
                let last = value.count - 1
                ref.child("users").child(user.uid).child("trips").child("trip\(last)").updateChildValues(["completed":true])
            })
            
            ref.child("users").child(driverUid).child("trips").observeSingleEvent(of: .value, with: {(snaps) in
                guard let value = snaps.value as? NSDictionary else {
                    return
                }
                let last = value.count - 1
                ref.child("users").child(self.driverUid).child("trips").child("trip\(last)").updateChildValues(["completed":true])
            })
        }
    }
    
    func clearAllData() {
        fromLocation = nil
        toLocation = nil
        totalCost = 0
        vehicleType = .bike
        //        paymentMethod = .cash
        isActive = false
        
    }
    
//    func findDriver(action: @escaping (String) -> Void) {
//        let ref = Database.database().reference()
//
//        ref.child("users").observeSingleEvent(of: .value, with: {(snaps) in
//            guard let value = snaps.value as? NSDictionary else {
//                return
//            }
//            for item in value {
//                print(item.value as? NSDictionary)
//                if let itemList = item.value as? NSDictionary,
//                   let type = itemList["usrType"] as? String,
//                   type == "driver",
//                   let active = itemList["isActive"] as? Int,
//                   active == 1,
//                   let lat = itemList["lat"] as? Double,
//                   let lng = itemList["lng"] as? Double,
//                   let fromloc = self.fromLocation,
//                   let driverUid = item.key as? String
//                {
//                    let distanceInMeters = fromloc.location.distance(from: CLLocation(latitude:lat, longitude: lng))
//                    if distanceInMeters < 5000 {
//                        self.driverUid = driverUid
//
//                        let uid = Auth.auth().currentUser!.uid
//                        ref.child("users").child(self.driverUid).updateChildValues(["isActive": false])
//
//                        if let user = Auth.auth().currentUser {
//                            self.updateTripForUser(driverUid, user.uid)
//                            self.updateTripForUser(user.uid, driverUid)
//                        }
//                        action(self.driverUid)
//                        break
//                    }
//                }
//
//            }
//        })
//
//    }
    
//    func updateTripForUser(_ uid: String, _ partnerUid: String) {
//        let ref = Database.database().reference()
//        var data = ["isActive": true,
//                    "cost":BookingDataHandle.shared.totalCost,
//                    "from":["lat": self.fromLocation?.coordinate.latitude ?? 0,
//                            "lng":  self.fromLocation?.coordinate.longitude ?? 0],
//                    "to":["lat": self.toLocation?.coordinate.latitude ?? 0,
//                          "lng": self.toLocation?.coordinate.longitude ?? 0],
//                    "vehicleType":BookingDataHandle.shared.vehicleType.rawValue,
//                    "paymentMethod":BookingDataHandle.shared.paymentMethod.rawValue,
//                    "partnerUid": partnerUid] as [String : Any]
//
//        ref.child("users").child(uid).child("trips").observeSingleEvent(of: .value, with: {(snaps) in
//            guard let value = snaps.value as? NSDictionary else {
//                ref.child("users").child(uid).child("trips").child("trip\(0)").setValue(data)
//                return
//            }
//            let last = value.count
//            ref.child("users").child(uid).child("trips").child("trip\(last)").setValue(data)
//        })
//
//    }
    
    func getDriverPos(action: @escaping (CLLocationCoordinate2D) -> Void ) {
        let ref = Database.database().reference()
        ref.child("users").child(self.driverUid).observeSingleEvent(of: .value, with: {(snaps) in
            guard let value = snaps.value as? NSDictionary else {
                return
            }
            let loc = CLLocationCoordinate2D(latitude: 1, longitude: 1)
            action(loc)
            return
        })
    }
    
}
