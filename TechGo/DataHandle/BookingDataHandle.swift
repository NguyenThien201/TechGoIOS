//
//  BookingDataHandle.swift
//  TechGo
//
//  Created by Thiện Nguyễn on 06/01/2021.
//

import Foundation
import LocationPicker
import Firebase
import FirebaseAuth
import FirebaseDatabase
import CoreLocation
import MapKit

enum VehicleType: String {
    case car = "car"
    case bike = "bike"
}

enum TripPaymentType: String  {
    case cash = "cash"
    case wallet = "wallet"
}

class BookingDataHandle {
    static var shared: BookingDataHandle = BookingDataHandle()
    
    var fromLocation: Location? = nil
    var toLocation: Location? = nil
    
    var driverLocation: Location? = nil
    var driverUid: String = ""
    
    var totalCost: Int = 0
    
    var vehicleType: VehicleType = .bike
    var paymentMethod: TripPaymentType = .cash
    var isActive: Bool = false
    
    var driverData = UserData()
    var willCancelTrip = false
    
    func clearAllData() {
        fromLocation = nil
        toLocation = nil
        totalCost = 0
        vehicleType = .bike
        //        paymentMethod = .cash
        isActive = false
        
    }
    
    func findDriver(action: @escaping (String) -> Void) {
        let ref = Database.database().reference()
        
        ref.child("users").observeSingleEvent(of: .value, with: {(snaps) in
            guard let value = snaps.value as? NSDictionary else {
                return
            }
            for item in value {
                if self.willCancelTrip {
                    self.willCancelTrip = false
                    break
                }
                print(item.value as? NSDictionary)
                if let itemList = item.value as? NSDictionary,
                   let type = itemList["usrType"] as? String,
                   type == "driver",
                   let active = itemList["isActive"] as? Int,
                   active == 1,
                   let lat = itemList["lat"] as? Double,
                   let lng = itemList["lng"] as? Double,
                   let fromloc = self.fromLocation,
                   let driverUid = item.key as? String
                {
                    let distanceInMeters = fromloc.location.distance(from: CLLocation(latitude:lat, longitude: lng))
                    if distanceInMeters < 5000 {
                        self.driverUid = driverUid
                        
                        let uid = Auth.auth().currentUser!.uid
                        ref.child("users").child(self.driverUid).updateChildValues(["isActive": false])
                        
                        if let user = Auth.auth().currentUser {
                            self.updateTripForUser(driverUid, user.uid)
                            self.updateTripForUser(user.uid, driverUid)
                        }
                        self.checkDriverCancelStatus()
                        self.configCustomerData(action: {
                            action(self.driverUid)
                        })
                    
                        break
                    }
                }
            }
        })
        
    }
    func configCustomerData(action: @escaping () -> Void) {
        let ref = Database.database().reference()
        if self.driverUid.isEmpty { return }
        ref.child("users").child(self.driverUid).observeSingleEvent(of: .value, with: {(snaps) in
            guard let value = snaps.value as? NSDictionary else {
                return
            }
            self.driverData.displayName = value["displayName"] as? String ?? ""
            self.driverData.phoneNumber = value["phoneNumber"] as? String ?? ""
            self.driverData.vehicleType = value["vehicle"] as? String ?? ""
            
            if let lat = value["lat"] as? Double,
               let lng = value["lng"] as? Double {
                
                self.driverLocation = Location.init(name: nil, location: CLLocation(latitude: lat, longitude: lng), placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: lat, longitude: lng), addressDictionary: nil))
                action()
                return
            }
            action()
            return
        })
//        action()
    }
    
    func updateTripForUser(_ uid: String, _ partnerUid: String) {
        let ref = Database.database().reference()
        var data = ["isActive": true,
                    "cost":BookingDataHandle.shared.totalCost,
                    "from":["lat": self.fromLocation?.coordinate.latitude ?? 0,
                            "lng":  self.fromLocation?.coordinate.longitude ?? 0],
                    "to":["lat": self.toLocation?.coordinate.latitude ?? 0,
                          "lng": self.toLocation?.coordinate.longitude ?? 0],
                    "vehicleType":BookingDataHandle.shared.vehicleType.rawValue,
                    "paymentMethod":BookingDataHandle.shared.paymentMethod.rawValue,
                    "partnerUid": partnerUid,
                    "driverCancel": false,
                    "picked":false,
                    "completed":false] as [String : Any]
        
        ref.child("users").child(uid).child("trips").observeSingleEvent(of: .value, with: {(snaps) in
            guard let value = snaps.value as? NSDictionary else {
                ref.child("users").child(uid).child("trips").child("trip\(0)").setValue(data)
                return
            }
            let last = value.count
            ref.child("users").child(uid).child("trips").child("trip\(last)").setValue(data)
        })
        
    }
    
    func checkDriverCancelStatus() {
        var timer: Timer!
        func block() {
            if !self.willCancelTrip {
                let ref = Database.database().reference()
                if self.driverUid.isEmpty { return }
                ref.child("users").child(self.driverUid).child("trips").observeSingleEvent(of: .value, with: {(snaps) in
                    guard let value = snaps.value as? NSDictionary else {
                        return
                    }
                    let last = value.count
                    ref.child("users").child(self.driverUid).child("trips").child("trip\(last - 1)").observeSingleEvent(of: .value, with: {(snaps) in
                        guard let value = snaps.value as? NSDictionary else {
                            return
                        }
                        self.willCancelTrip = (value["driverCancel"] as? Int ?? 0)  == 1 ? true : false
                        if self.willCancelTrip {
                            timer?.invalidate()
                        }
                        
                    })
                })
                
            }
        }
        timer = Timer.scheduledTimer(withTimeInterval: 2, repeats: true, block: {
            _ in
            block()
        })
    }
    
        
    func getDriverPos(action: @escaping (CLLocationCoordinate2D) -> Void ) {
        let ref = Database.database().reference()
        if self.driverUid.isEmpty { return }
        ref.child("users").child(self.driverUid).observeSingleEvent(of: .value, with: {(snaps) in
            guard let value = snaps.value as? NSDictionary else {
                return
            }
            if let lat = value["lat"] as? Double,
               let lng = value["lng"] as? Double {
                self.driverLocation = Location.init(name: nil, location: CLLocation(latitude: lat, longitude: lng), placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: lat, longitude: lng), addressDictionary: nil))
                action(self.driverLocation!.coordinate)
            }
            return
        })
    }
    
    func getTripStatus(action: @escaping (TripStatus) -> Void ) {
        let ref = Database.database().reference()
        if let user = Auth.auth().currentUser {
            ref.child("users").child(user.uid).child("trips").observeSingleEvent(of: .value, with: {(snaps) in
                guard let value = snaps.value as? NSDictionary else {
                    return
                }
                let last = value.count - 1
                ref.child("users").child(user.uid).child("trips").child("trip\(last)").observeSingleEvent(of: .value, with: {(snaps) in
                    guard let value = snaps.value as? NSDictionary else {
                        return
                    }
                    if let completed = value["completed"] as? Int,
                       completed == 1 {
                        action(TripStatus.completed)
                        return
                    }
                    if let picked = value["picked"] as? Int,
                       picked == 1 {
                        action(TripStatus.picked)
                        return
                    }
                    action(.none)
                })
               
                return
            })
        }
    }
    
}

enum TripStatus {
    case picked, completed, none
}
