//
//  UserDataHandler.swift
//  TechGo
//
//  Created by Thiện Nguyễn on 22/12/2020.
//

import Foundation
import Firebase
import FirebaseAuth
import FirebaseDatabase
import CoreLocation

enum UserType: String {
    case customer = "customer"
    case driver = "driver"
}
class UserData {
    static var shared = UserData()
//    var userName: String = ""
    var userType: UserType = .customer
    var location: CLLocationCoordinate2D? = nil
    var displayName: String = ""
    var password: String = ""
    var phoneNumber: String = ""
    var email: String = ""
    var address: String = ""
    var dob: String = ""
    var vehicleType: String = ""
    var wallet: Int = 0
    
    func signUp() {
        Auth.auth().createUser(withEmail: email, password: password) { authResult, error in
        }
    }
    
    func fetchingData(_ action: @escaping () -> Void) {
        let ref = Database.database().reference()
        
        
        if let user = Auth.auth().currentUser {
            ref.child("users").child(user.uid).updateChildValues(["isActive": true])
            ref.child("users").child(user.uid).observeSingleEvent(of: .value, with: {(snaps) in
                guard let value = snaps.value as? NSDictionary else {
                    self.userType = .customer
                    return
                }
                self.userType = UserType(rawValue: value["usrType"] as? String  ?? "customer" ) ?? UserType.customer
                self.displayName = value["displayName"] as? String ?? ""
                self.phoneNumber = value["phoneNumber"] as? String ?? ""
                self.vehicleType = value["vehicle"] as? String ?? ""
                self.wallet = value["wallet"] as? Int ?? 0
                action()
            })
        }
    }
    
    func configUserType() {
        let ref = Database.database().reference()
        
        
        if let user = Auth.auth().currentUser {
            ref.child("users").child(user.uid).updateChildValues(["isActive": true])
            ref.child("users").child(user.uid).observeSingleEvent(of: .value, with: {(snaps) in
                guard let value = snaps.value as? NSDictionary else {
                    self.userType = .customer
                    return
                }
                self.userType = UserType(rawValue: value["usrType"] as? String  ?? "customer" ) ?? UserType.customer
            })
        }
    }
    func checkExistedUser(execute: @escaping (Bool) -> ()) {
        let databaseRef = Database.database().reference()
        
        databaseRef.child("users").observeSingleEvent(of: .value, with: { (snapshot) in
            if let userList = snapshot.children.allObjects as? [DataSnapshot] {
                for user in userList where user.hasChild(self.email) {
                    execute(true)
                    print("print after execute")
                    return
                }
            }
            execute(false)
            print("print after execute")
            return
        }) { (error) in
            print(error.localizedDescription)
        }
        execute(false)
        print("print after execute")
        execute(false)
        print("print after execute")
    }
    
    
    func postSignupUserData() {
        Auth.auth().createUser(withEmail: self.email, password: self.password) { (res, err) in
            if (err != nil) {
                print(err)
                return
            }
            print(res)
            var ref: DatabaseReference!
            ref = Database.database().reference()
            if let userID = res?.user.uid {
                ref.child("users").child(userID).setValue(["displayName": self.displayName,
                                                           "password": self.password,
                                                           "phoneNumber": self.phoneNumber,
                                                           "email": self.email,
                                                           "usrType": self.userType.rawValue,
                                                           "isActive":true,
                                                           "vehicle": self.vehicleType,
                                                           "wallet": 0,
                                                           "lat":0,
                                                           "lng":0])
            }
            
        }
        
        //            ref.child("users").child(userID!).observeSingleEvent(of: .value, with: { (snapshot) in
        //              // Get user value
        //              let value = snapshot.value as? NSDictionary
        //              let username = value?["username"] as? String ?? ""
        //              let user = User(username: username)
        //
        //              // ...
        //              }) { (error) in
        //                print(error.localizedDescription)
        //            }
    }
    
    func updateLocation() {
        var ref: DatabaseReference!
        ref = Database.database().reference()
        if let currentUser = Auth.auth().currentUser {
            ref.child("users").child(currentUser.uid).updateChildValues([
                                                                            "lat":self.location?.latitude ?? 0,
                                                                            "lng":self.location?.longitude ?? 0])
        }
    }
}

