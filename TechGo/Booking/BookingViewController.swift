//
//  BookingViewController.swift
//  TechGo
//
//  Created by Thiện Nguyễn on 04/01/2021.
//

import Foundation
import SnapKit
import MaterialComponents
import LocationPicker
import MapKit
import CoreLocation

class BookingViewController: CustomViewcontroller, LoginUIProtocol {
    var tripData = BookingDataHandle()
    
    var buttonHeight: CGFloat = 60
    
    lazy var fromLbl: MDCOutlinedTextField = getTextField(with: "from".localized())
    lazy var toLbl: MDCOutlinedTextField = getTextField(with: "to".localized())
    
    
    var bikeButton = MDCButton().then({
        $0.setTitle("bike".localized(), for: .normal)
        $0.setTitleFont(UIFont.systemFont(ofSize: 15, weight: .semibold), for: .normal)
        $0.setBackgroundColor(.lighterPrimaryColor(), for: .normal)
        $0.setBackgroundColor(.primaryColor(), for: .selected)
//        $0.layer.cornerRadius = 16
        
        $0.addTarget(self, action: #selector(bikeDidClick), for: .touchUpInside)
    })
    var carButton = MDCButton().then({
        $0.setTitle("car".localized(), for: .normal)
        $0.setTitleFont(UIFont.systemFont(ofSize: 15, weight: .semibold), for: .normal)
        $0.setBackgroundColor(.lighterPrimaryColor(), for: .normal)
        $0.setBackgroundColor(.primaryColor(), for: .selected)
//        $0.layer.cornerRadius = 16
        $0.addTarget(self, action: #selector(carDidClick), for: .touchUpInside)
    })
    var paymentButton = MDCButton().then({
        $0.setTitle("paymentMethod".localized(), for: .normal)
        $0.setTitleFont(UIFont.systemFont(ofSize: 12, weight: .semibold), for: .normal)
        $0.setBackgroundColor(.primaryColor(), for: .normal)
//        $0.layer.cornerRadius = 16
        $0.addTarget(self, action: #selector(changePaymentMethod), for: .touchUpInside)
    })
    var bookButton = MDCButton().then({
        $0.setTitle("book".localized(), for: .normal)
        $0.setTitleFont(UIFont.systemFont(ofSize: 15, weight: .semibold), for: .normal)
        $0.setBackgroundColor(.primaryColor(), for: .normal)
        $0.addTarget(self, action: #selector(bookDidClick), for: .touchUpInside)
    })
    
    var paymentLabel = UILabel().then({
        $0.text = "cost".localized()
    })
    var totalCostLabel = UILabel().then({
        $0.font = UIFont.systemFont(ofSize: 18, weight: .bold)
        $0.textAlignment = .right
    })
    var paymentMethodLabel = UILabel().then({
        $0.text = "cash".localized()
        $0.font = UIFont.systemFont(ofSize: 18, weight: .bold)
        $0.textAlignment = .center
    })
    
    @objc
    func bikeDidClick() {
        self.carButton.isSelected = false
        self.bikeButton.isSelected = true
        tripData.vehicleType = .bike
    }
    
    
    @objc
    func carDidClick() {
        self.carButton.isSelected = true
        self.bikeButton.isSelected = false
        tripData.vehicleType = .car
    }
    
    @objc
    func bookDidClick() {
        BookingDataHandle.shared = self.tripData
    
        let vc = FindingDriverViewController()
        vc.modalPresentationStyle = .fullScreen
        self.performPresentAnimation()
        self.present(vc, animated: false, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addBackButton()
        self.addLogoImg()
       self.view.backgroundColor = .lighterPrimaryColor()
        fromLbl.delegate = self
        holderView = self.configHolderView()
        holderView.then({
            $0.addSubview(fromLbl)
            $0.addSubview(toLbl)
            $0.addSubview(bikeButton)
            $0.addSubview(carButton)
            $0.addSubview(paymentLabel)
            $0.addSubview(totalCostLabel)
            $0.addSubview(paymentButton)
            $0.addSubview(bookButton)
            $0.addSubview(paymentMethodLabel)
        })
        
        fromLbl.addTarget(self, action: #selector(showMapPicker(_:)), for: .touchDown)
//        fromLbl.clearsOnBeginEditing = true
        toLbl.addTarget(self, action: #selector(showMapPicker(_:)), for: .touchDown)
//        toLbl.clearsOnBeginEditing = true
        self.fromLbl.snp.makeConstraints({
            $0.top.equalTo(holderView.snp.top).offset(topPadding)
            $0.centerX.equalToSuperview()
            $0.width.equalToSuperview().inset(20)
            
        })
        
        self.toLbl.snp.makeConstraints({
            $0.top.equalTo(fromLbl.snp.bottom).offset(topPadding)
            $0.centerX.equalToSuperview()
            $0.width.equalToSuperview().inset(20)
            
        })
        
        bikeButton.snp.makeConstraints({
            $0.top.equalTo(toLbl.snp.bottom).offset(topPadding)
            $0.left.equalToSuperview().inset(topPadding)
//            $0.height.equalTo(buttonHeight)
        })
        
        carButton.snp.makeConstraints({
            $0.width.equalTo(bikeButton.snp.width)
            $0.left.equalTo(bikeButton.snp.right).offset(topPadding)
            $0.bottom.top.equalTo(bikeButton)
            $0.right.equalToSuperview().inset(topPadding)
//            $0.height.equalTo(buttonHeight)
        })
        
        paymentLabel.snp.makeConstraints({
            $0.top.equalTo(bikeButton.snp.bottom).offset(topPadding)
            $0.left.equalToSuperview().inset(topPadding)
        })
        
        totalCostLabel.snp.makeConstraints({
            $0.top.equalTo(bikeButton.snp.bottom).offset(topPadding)
            $0.right.equalToSuperview().inset(topPadding)
            $0.left.equalTo(paymentLabel.snp.right)
            $0.width.equalTo(paymentLabel.snp.width)
        })
        
        paymentMethodLabel.snp.makeConstraints({
            $0.top.equalTo(paymentLabel.snp.bottom).offset(topPadding)
            $0.left.equalToSuperview().inset(topPadding)
        })
        
        
        paymentButton.snp.makeConstraints({
            $0.width.equalTo(paymentLabel.snp.width)
            $0.left.equalTo(paymentMethodLabel.snp.right).offset(topPadding)
            $0.right.equalToSuperview().inset(topPadding)
            $0.top.equalTo(paymentLabel.snp.bottom).offset(topPadding)
        })
        
        self.bookButton.snp.makeConstraints({
            $0.top.equalTo(paymentButton.snp.bottom).offset(topPadding)
            $0.left.right.bottom.equalToSuperview().inset(topPadding)
        })
        
        bikeDidClick()
    }
    
    override func updateTextFieldLanguage() {
        
    }
    
    @objc func showMapPicker(_ textField: MDCOutlinedTextField) {
        let vc = TechGoLocationPickerVC()
        
        if textField.isEqual(self.fromLbl) {
            vc.location = self.tripData.fromLocation
        } else {
            vc.location = self.tripData.toLocation
        }
        
        vc.completion = { location in
            textField.text = location?.name
            if textField.isEqual(self.fromLbl) {
                self.tripData.fromLocation = location
            } else {
                self.tripData.toLocation = location
            }
            self.updateCost()
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    func updateCost() {
        guard let from = tripData.fromLocation,
              let to = tripData.toLocation else { return }
        
        let distanceInMeters = from.location.distance(from: to.location)
        self.tripData.totalCost = Int(distanceInMeters) / 100 * 1200
        self.totalCostLabel.text = "\(self.tripData.totalCost)" + "vnd".localized()
        
    }
    
    @objc
    func changePaymentMethod() {
        let alertController = MDCAlertController(title: "payment".localized(), message: "change_payment_method".localized())
        let cash = MDCAlertAction(title: "cash".localized().uppercased()) { _ in
            self.tripData.paymentMethod = .cash
            self.paymentMethodLabel.text = self.tripData.paymentMethod.rawValue.localized()
            alertController.dismiss(animated: false, completion: {})
        }
        
        let wallet = MDCAlertAction(title: "wallet".localized().uppercased()) { _ in
            self.tripData.paymentMethod = .wallet
            self.paymentMethodLabel.text = self.tripData.paymentMethod.rawValue.localized()
            alertController.dismiss(animated: false, completion: {})
        }
        alertController.titleFont = UIFont.systemFont(ofSize: 20, weight: .medium)
        alertController.buttonTitleColor = .primaryColor()
        alertController.addAction(cash)
        alertController.addAction(wallet)
        alertController.cornerRadius = 8
        present(alertController, animated:true, completion: {})
    }
    
    
}
extension BookingViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return false
    }
}
