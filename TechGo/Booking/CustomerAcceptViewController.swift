//
//  CustomerAcceptViewController.swift
//  TechGo
//
//  Created by Thiện Nguyễn on 06/01/2021.
//

import Foundation
import SnapKit
import MaterialComponents
import LocationPicker
import MapKit
import CoreLocation

private extension MKMapView {
    func centerToLocation(
        _ location: CLLocation,
        regionRadius: CLLocationDistance = 1000
    ) {
        let coordinateRegion = MKCoordinateRegion(
            center: location.coordinate,
            latitudinalMeters: regionRadius,
            longitudinalMeters: regionRadius)
        setRegion(coordinateRegion, animated: true)
    }
}

class CustomerAcceptViewController: CustomViewcontroller, LoginUIProtocol {
    var tripData: DriverTripDataHandler {
        return DriverTripDataHandler.shared
    }
    var driverPos = Artwork(title: "driver", locationName: nil, discipline: nil, coordinate: UserData.shared.location!).then({
        $0.type = .driver
    })
    var mapView: MKMapView = MKMapView().then({
        $0.layer.cornerRadius = 8
    })
    let initialLocation = CLLocation(latitude: 21.282778, longitude: -157.829444)
    var buttonHeight: CGFloat = 60
    
    lazy var atLbl: MDCOutlinedTextField = getTextField(with: "from".localized())
    
    var driverFounLbl: UILabel = UILabel().then({
        $0.text = "customer_found".localized() 
        $0.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        $0.textAlignment = .center
    })
    
    var driverName: UILabel = UILabel().then({
        $0.text = "customerName".localized() + DriverTripDataHandler.shared.userData.displayName
        $0.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
    })
    
    var vehicleType: UILabel = UILabel().then({
        $0.text = "vehicle_type".localized() + UserData.shared.vehicleType.localized()
        $0.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
    })
    
    var phoneNumber: UILabel = UILabel().then({
        $0.text = "phone_number".localized() + ": " + DriverTripDataHandler.shared.userData.phoneNumber
        $0.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
    })
    
    var arriveLbl: UILabel = UILabel().then({
        $0.text = "cost".localized() + DriverTripDataHandler.shared.userData.phoneNumber
        $0.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
    })
    
    var costLbl: UILabel = UILabel().then({
        $0.text = "cost".localized() + "\(DriverTripDataHandler.shared.totalCost)"
        $0.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
    })
    
    var pickingPos: Artwork!
    
    
    var acceptButton = MDCButton().then({
        $0.setTitle("picked".localized(), for: .normal)
        $0.setTitleFont(UIFont.systemFont(ofSize: 15, weight: .semibold), for: .normal)
        $0.setBackgroundColor(.primaryColor(), for: .normal)
        $0.addTarget(self, action: #selector(acceptDidClick), for: .touchUpInside)
    })
    
    var cancelButton = MDCButton().then({
        $0.setTitle("cancel".localized(), for: .normal)
        $0.setTitleFont(UIFont.systemFont(ofSize: 15, weight: .semibold), for: .normal)
        $0.setBackgroundColor(.primaryColor(), for: .normal)
        $0.addTarget(self, action: #selector(cancelDidClick), for: .touchUpInside)
    })
    
    
    @objc
    func cancelDidClick() {
        let alertController = MDCAlertController(title: "driver_cancel".localized(), message: "do you want to find new driver".localized())
        let cash = MDCAlertAction(title: "Cancel".localized().uppercased()) { _ in
            alertController.dismiss(animated: false, completion: {
                self.backTapped()
            })
            
        }
        
        let wallet = MDCAlertAction(title: "Confirm".localized().uppercased()) { _ in
            alertController.dismiss(animated: false, completion: {
                self.tripData.cancelTrip()
            })
        }
        alertController.titleFont = UIFont.systemFont(ofSize: 20, weight: .medium)
        alertController.buttonTitleColor = .primaryColor()
        alertController.addAction(wallet)
        alertController.addAction(cash)
        alertController.cornerRadius = 8
        present(alertController, animated:true, completion: {})
    }
    
    var isPicked: Bool = false
    
    @objc
    func acceptDidClick() {
        if !isPicked {
            isPicked = true
            self.tripData.pickedCustomer()
            self.acceptButton.setTitle("completed".localized(), for: .normal)
            self.showToLoc()
        } else {
            self.tripData.completedTrip()
            self.present(FlashViewController.getTabbarcontroller(), animated: true, completion: nil)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        self.addBackButton()
        //        self.addLogoImg()
        self.view.backgroundColor = .lighterPrimaryColor()
        
        holderView = self.configHolderView()
        
        holderView.then({
            $0.addSubview(driverFounLbl)
            $0.addSubview(atLbl)
            $0.addSubview(mapView)
            $0.addSubview(driverName)
            $0.addSubview(vehicleType)
            $0.addSubview(phoneNumber)
            $0.addSubview(arriveLbl)
            $0.addSubview(costLbl)
            $0.addSubview(cancelButton)
            $0.addSubview(acceptButton)
            
        })
        
        driverFounLbl.snp.makeConstraints({
            $0.top.equalTo(holderView.snp.top).offset(topPadding)
            $0.centerX.equalToSuperview()
            $0.width.equalToSuperview().inset(20)
        })
        
        atLbl.snp.makeConstraints({
            $0.top.equalTo(driverFounLbl.snp.bottom).offset(topPadding)
            $0.centerX.equalToSuperview()
            $0.width.equalToSuperview().inset(20)
        })
        
        self.mapView.snp.makeConstraints({
            $0.top.equalTo(atLbl.snp.bottom).offset(topPadding)
            $0.centerX.equalToSuperview()
            $0.width.equalToSuperview().inset(20)
            $0.height.equalTo(self.mapView.snp.width)
        })
        
        self.driverName.snp.makeConstraints({
            $0.top.equalTo(mapView.snp.bottom).offset(topPadding)
            $0.centerX.equalToSuperview()
            $0.width.equalToSuperview().inset(20)
        })
        vehicleType.snp.makeConstraints({
            $0.top.equalTo(driverName.snp.bottom)
            $0.centerX.equalToSuperview()
            $0.width.equalToSuperview().inset(20)
        })
        phoneNumber.snp.makeConstraints({
            $0.top.equalTo(vehicleType.snp.bottom)
            $0.centerX.equalToSuperview()
            $0.width.equalToSuperview().inset(20)
        })
        arriveLbl.snp.makeConstraints({
            $0.top.equalTo(phoneNumber.snp.bottom)
            $0.centerX.equalToSuperview()
            $0.width.equalToSuperview().inset(20)
        })
        costLbl.snp.makeConstraints({
            $0.top.equalTo(arriveLbl.snp.bottom)
            $0.centerX.equalToSuperview()
            $0.width.equalToSuperview().inset(20)
        })
        
        self.cancelButton.snp.makeConstraints({
            $0.top.equalTo(costLbl.snp.bottom).offset(topPadding)
            $0.left.bottom.equalToSuperview().inset(topPadding)
        })
        
        acceptButton.snp.makeConstraints({
            $0.top.equalTo(costLbl.snp.bottom).offset(topPadding)
            $0.right.bottom.equalToSuperview().inset(topPadding)
            $0.left.equalTo(cancelButton.snp.right).offset(topPadding)
            $0.width.equalTo(self.cancelButton.snp.width)
        })
        self.mapView.delegate = self
        pickingPos = Artwork(
            title: "Picking location",
            locationName: "The driver is going to pick you up here",
            discipline: "",
            coordinate: (tripData.fromLocation?.location.coordinate)!)
        
        self.mapView.addAnnotation(pickingPos)
        self.mapView.addAnnotation(driverPos)
        
        runAutoUpdateLoc()
    }
    
    func showToLoc() {
        self.mapView.removeAnnotation(pickingPos)
        pickingPos = Artwork(
            title: "Drop off location",
            locationName: "",
            discipline: "",
            coordinate: (tripData.toLocation?.location.coordinate)!)
        self.mapView.addAnnotation(pickingPos)
    }
    
    func runAutoUpdateLoc() {
        if UserData.shared.userType == .customer {
            let _ = Timer.scheduledTimer(withTimeInterval: 2, repeats: true, block: {
                _ in
                BookingDataHandle.shared.getDriverPos(action: { location in
                    print(location)
                    UIView.animate(withDuration: 1.0, animations: {
                        self.driverPos.coordinate = location
                    })
                })
            })
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //        let region = MKCoordinateRegion(
        //            center: (tripData.fromLocation?.location.coordinate)!,
        //            latitudinalMeters: 10000,
        //            longitudinalMeters: 10000)
        //
        //        self.mapView.setCameraBoundary(
        //            MKMapView.CameraBoundary(coordinateRegion: region),
        //            animated: true)
        self.mapView.setCenter((tripData.fromLocation?.location.coordinate)!, animated: false)
        let zoomRange = MKMapView.CameraZoomRange(maxCenterCoordinateDistance: 10000)
        self.mapView.setCameraZoomRange(zoomRange, animated: true)
    }
    
    override func updateTextFieldLanguage() {
        
    }
    
    
    
    
}
extension CustomerAcceptViewController: UITextFieldDelegate, MKMapViewDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return false
    }
    // 1
    func mapView(
        _ mapView: MKMapView,
        viewFor annotation: MKAnnotation
    ) -> MKAnnotationView? {
        // 2
        guard let annotation = annotation as? Artwork else {
            return nil
        }
        
        // 3
        let identifier = "artwork"
        var view: MKMarkerAnnotationView
        // 4
        if let dequeuedView = mapView.dequeueReusableAnnotationView(
            withIdentifier: identifier) as? MKMarkerAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
        } else {
            // 5
            view = MKMarkerAnnotationView(
                annotation: annotation,
                reuseIdentifier: identifier)
            view.glyphImage = UIImage(named: "logoIcon")
            view.markerTintColor = annotation.type == .customer ? UIColor.primaryColor() : UIColor.darkerPrimaryColor()
            view.canShowCallout = false
            
            //      view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5, y: 5)
            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        return view
    }
}

