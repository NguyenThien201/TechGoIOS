//
//  TechGoLocationPickerVC.swift
//  TechGo
//
//  Created by Thiện Nguyễn on 05/01/2021.
//

import Foundation
import LocationPicker
import MapKit
import UIKit

class TechGoLocationPickerVC: UINavigationController {
    let locationVc = LocationPickerViewController()
    var completion: ((Location?) -> ())?
    var location: Location? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let locationPicker = LocationPickerViewController()

        // you can optionally set initial location
        let placemark = MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: 10.763134334307447, longitude: 106.68223607086696), addressDictionary: nil)
        let location = Location(name: "Đại học Khoa Học Tự Nhiên", location: CLLocation(latitude: 10.763134334307447, longitude: 106.68223607086696), placemark: placemark)
        locationPicker.location = location

        // button placed on right bottom corner
        locationPicker.showCurrentLocationButton = true // default: true
        if let loc = self.location {
            locationPicker.location = loc
        }
        // default: navigation bar's `barTintColor` or `UIColor.white`
        locationPicker.currentLocationButtonBackground = .blue

        // ignored if initial location is given, shows that location instead
        locationPicker.showCurrentLocationInitially = true // default: true

        locationPicker.mapType = .standard // default: .Hybrid

        // for searching, see `MKLocalSearchRequest`'s `region` property
        locationPicker.useCurrentLocationAsHint = true // default: false

        locationPicker.searchBarPlaceholder = "Search places" // default: "Search or enter an address"

        locationPicker.searchHistoryLabel = "Previously searched" // default: "Search History"

        // optional region distance to be used for creation region when user selects place from search results
        locationPicker.resultRegionDistance = 500 // default: 600
//        locationPicker.modalPresentationStyle = .fullScreen
        locationPicker.completion = completion
        
        self.pushViewController(locationPicker, animated: true)
    }
    
}
