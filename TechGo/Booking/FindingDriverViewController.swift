//
//  FindingDriverViewController.swift
//  TechGo
//
//  Created by Thiện Nguyễn on 06/01/2021.
//

import Foundation
import SnapKit
import MaterialComponents
import LocationPicker
import MapKit
import CoreLocation

class FindingDriverViewController: CustomViewcontroller, LoginUIProtocol {
    var tripData = BookingDataHandle()
    
    var buttonHeight: CGFloat = 60
    
    var dotCount = 0
    
    var cancelButton = MDCButton().then({
        $0.setTitle("cancel".localized(), for: .normal)
        $0.setTitleFont(UIFont.systemFont(ofSize: 15, weight: .semibold), for: .normal)
        $0.setBackgroundColor(.primaryColor(), for: .normal)
        $0.addTarget(self, action: #selector(cancelDidClick), for: .touchUpInside)
    })
    
    var findingDriverLabel = UILabel().then({
        $0.text = "findingDriver".localized()
        $0.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
    })
    
    var findingLogo: UIImageView = UIImageView(image: UIImage(named: "findLogo")).then({
        $0.contentMode = .scaleAspectFit
    })
    var timer = Timer()
    @objc
    func cancelDidClick() {
        let alertController = MDCAlertController(title: "cancel_trip".localized(), message: "do you want to cancel the trip".localized())
        let cash = MDCAlertAction(title: "Cancel".localized().uppercased()) { _ in
            alertController.dismiss(animated: false, completion: {})
        }
        
        let wallet = MDCAlertAction(title: "Confirm".localized().uppercased()) { _ in
            
            BookingDataHandle.shared.willCancelTrip = true
            alertController.dismiss(animated: false, completion: {
                self.timer.invalidate()
                self.backTapped()
            })
        }
        alertController.titleFont = UIFont.systemFont(ofSize: 20, weight: .medium)
        alertController.buttonTitleColor = .primaryColor()
        alertController.addAction(wallet)
        alertController.addAction(cash)
        alertController.cornerRadius = 8
        present(alertController, animated:true, completion: {})
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if BookingDataHandle.shared.willCancelTrip {
            BookingDataHandle.shared.willCancelTrip = false
            let alertController = MDCAlertController(title: "driver_cancel".localized(), message: "do you want to find new driver".localized())
            let cash = MDCAlertAction(title: "Cancel".localized().uppercased()) { _ in
                alertController.dismiss(animated: false, completion: {
                                            self.timer.invalidate()
                                            self.backTapped()
                })

            }
            
            let wallet = MDCAlertAction(title: "Confirm".localized().uppercased()) { _ in
                
                alertController.dismiss(animated: false, completion: {
                    self.performFindriver()
                })
            }
            alertController.titleFont = UIFont.systemFont(ofSize: 20, weight: .medium)
            alertController.buttonTitleColor = .primaryColor()
            alertController.addAction(wallet)
            alertController.addAction(cash)
            alertController.cornerRadius = 8
            present(alertController, animated:true, completion: {})
        } else {
            performFindriver()
        }
    }
    
    func performFindriver() {
        BookingDataHandle.shared.findDriver(action: {string in
            let vc = FoundADriverViewController()
            vc.modalPresentationStyle = .fullScreen
            self.performPresentAnimation()
            self.present(vc, animated: false, completion: {self.timer.invalidate()})
        })
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addLogoImg()
        
        self.view.backgroundColor = .lighterPrimaryColor()
        holderView = self.configHolderView()
        holderView.then({
            $0.addSubview(findingDriverLabel)
            $0.addSubview(findingLogo)
            $0.addSubview(cancelButton)
            
        })
        
        self.findingDriverLabel.snp.makeConstraints({
            $0.top.equalTo(holderView.snp.top).offset(topPadding)
            $0.centerX.equalToSuperview()
            $0.width.equalToSuperview().inset(20)
            
        })
        
        self.findingLogo.snp.makeConstraints({
            $0.top.equalTo(findingDriverLabel.snp.bottom).offset(topPadding)
            $0.centerX.equalToSuperview()
            $0.width.equalToSuperview().inset(20)
            $0.height.equalTo(self.findingLogo.snp.width)
        })
        
        self.cancelButton.snp.makeConstraints({
            $0.top.equalTo(findingLogo.snp.bottom).offset(topPadding)
            $0.left.right.bottom.equalToSuperview().inset(topPadding)
        })
       
         timer = Timer.scheduledTimer(withTimeInterval: 0.4, repeats: true, block: {
            _ in
            print("lolo")
            if self.dotCount == 0 {
                self.findingDriverLabel.text = "findingDriver".localized() + ".  "
                self.dotCount += 1
            } else if self.dotCount == 1 {
                self.findingDriverLabel.text = "findingDriver".localized() + ".. "
                self.dotCount += 1
            } else {
                self.findingDriverLabel.text = "findingDriver".localized() + "..."
                self.dotCount  = 0
            }
        })
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 20, execute: {
//            let alertController = MDCAlertController(title: "cancel_trip".localized(), message: "can not found driver near you".localized())
//            let wallet = MDCAlertAction(title: "Confirm".localized().uppercased()) { _ in
//                BookingDataHandle.shared.willCancelTrip = true
//                alertController.dismiss(animated: false, completion: {
//                    self.backTapped()
//                })
//            }
//            alertController.titleFont = UIFont.systemFont(ofSize: 20, weight: .medium)
//            alertController.buttonTitleColor = .primaryColor()
//            alertController.addAction(wallet)
//            alertController.cornerRadius = 8
//            self.present(alertController, animated:true, completion: {})
//        })
        
        
    }
    
    
}
extension FindingDriverViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return false
    }
}

