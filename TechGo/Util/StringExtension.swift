//
//  StringExtension.swift
//  TechGo
//
//  Created by Thiện Nguyễn on 14/12/2020.
//

import Foundation
class LangSetting {
        
    enum LangType: String {
        case vietnamese = "vi"
        case english = "en"
    }
    static func changeLange() {
        self.lang = self.lang == LangType.english ? .vietnamese : .english
    }
	static func getLangText() -> String {
		switch self.lang {
			case .english:
				return "English"
			default:
				return "Tiếng Việt"
		}
	}
    static var lang: LangType {
        set {
            UserDefaults.standard.set(newValue.rawValue, forKey: "langSetting")
        }
        get {
           
            if  let name = UserDefaults.standard.string(forKey: "langSetting"),
                let langSetting = LangType(rawValue: name)
            {
                return langSetting
            }
            return .english
        }
    }
    
}
extension String {
    func localized() -> String {
        
        let path = Bundle.main.path(forResource: LangSetting.lang.rawValue, ofType: "lproj")
            let bundle = Bundle(path: path!)

            return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
//        return NSLocalizedString(self, comment: "")
    }
}
