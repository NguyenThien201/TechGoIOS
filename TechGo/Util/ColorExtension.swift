//
//  ColorExtension.swift
//  TechGo
//
//  Created by Thiện Nguyễn on 14/12/2020.
//

import Foundation
import UIKit

extension UIColor {

    convenience init(red: Int, green: Int, blue: Int, a: CGFloat = 1.0) {
        self.init(
            red: CGFloat(red) / 255.0,
            green: CGFloat(green) / 255.0,
            blue: CGFloat(blue) / 255.0,
            alpha: a
        )
    }

    // let's suppose alpha is the first component (ARGB)
    convenience init(rgb: Int, alpha: CGFloat = 1.0) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF,
            a: alpha
        )
    }
//    620bd4
    public static func primaryColor() -> UIColor {
        return UIColor(rgb: 0xFFABAB).withAlphaComponent(1.0)
    }
    public static func lighterPrimaryColor() -> UIColor {
        return UIColor(rgb: 0xffcbc1).withAlphaComponent(1.0)
    }
    
    public static func darkerPrimaryColor() -> UIColor {
        return UIColor(rgb: 0xe66c6c).withAlphaComponent(1.0)
    }
    
//    ace7ff
}
