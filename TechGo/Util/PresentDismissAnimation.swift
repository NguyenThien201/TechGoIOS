//
//  PresentDismissAnimation.swift
//  TechGo
//
//  Created by Thiện Nguyễn on 19/12/2020.
//

import Foundation
import UIKit

class TechGoUtil {
    static func getPresentAnimation() -> CATransition {
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        return transition
    }
}
extension UIView {
    @discardableResult
    func applyGradient(colours: [UIColor]) -> CAGradientLayer {
        return self.applyGradient(colours: colours, locations: nil)
    }

    @discardableResult
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> CAGradientLayer {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
        return gradient
    }
}

//class ViewController: UIViewController {
//
//    @IBOutlet weak var btn: UIButton!
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        self.btn.applyGradient(colours: [.yellow, .blue])
//        self.view.applyGradient(colours: [.yellow, .blue, .red], locations: [0.0, 0.5, 1.0])
//    }
//
//
//}

class CusTabBarController: UITabBarController {
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.tabBar.frame = CGRect( x: 0, y: 0, width: 320, height: 50)  //example for iPhone 5
    }
}
