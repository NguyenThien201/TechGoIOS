//
//  NaviBackViewcontroller.swift
//  TechGow
//
//  Created by Thiện Nguyễn on 19/12/2020.
//

import Foundation
import UIKit
import SnapKit

class CustomViewcontroller: UIViewController {
    var backButton = UIButton()
    var changeLanguageLbl = UILabel()
    var btnHeight: CGFloat = 35
    var holderView: UIView!
    let logoImgView = UIImageView(image: UIImage(named: "logoIcon")).then({
        $0.contentMode = .scaleAspectFit
    })
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.changeLanguageLbl.text != LangSetting.lang.rawValue.uppercased() {
            self.changeLanguageLbl.text = LangSetting.lang.rawValue.uppercased()
            self.updateTextFieldLanguage()
        }
    }
    
    func performPresentAnimation() {
        if let wd = self.view.window {
            wd.layer.add(TechGoUtil.getPresentAnimation(), forKey: nil)
        }
        
    }
    
    func removeAllLayerAnimation() {
        self.view.window?.layer.removeAllAnimations()
    }
    
    func makeTextFieldFloat() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardNotification(notification:)),
                                               name: UIResponder.keyboardWillChangeFrameNotification,
                                               object: nil)
    }
    
    @objc func keyboardNotification(notification: NSNotification) {
         guard let userInfo = notification.userInfo else { return }

         let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
         let endFrameY = endFrame?.origin.y ?? 0
         let duration:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
         let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
         let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
         let animationCurve:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)

         if endFrameY >= UIScreen.main.bounds.size.height {
            view.frame.origin.y = 0.0
         } else {
            view.frame.origin.y =  -(endFrame?.size.height ?? 0.0)
         }

         UIView.animate(
           withDuration: duration,
           delay: TimeInterval(0),
           options: animationCurve,
           animations: { self.view.layoutIfNeeded() },
           completion: nil)
       }
    
deinit {
  NotificationCenter.default.removeObserver(self)
}
	
	func addLogoImg() { 
		self.view.addSubview(logoImgView)
		logoImgView.snp.makeConstraints({
			$0.left.right.equalToSuperview().inset(20)
			$0.top.equalToSuperview().inset(20)
			$0.height.equalTo(80)
		})
	}
	
	
    func addBackButton() {
        self.view.addSubview(backButton)
        backButton.setTitle("back".localized(), for: .normal)
        backButton.addTarget(self, action: #selector(backTapped), for: .touchUpInside)
        
        let labelView = UIView().then({
            $0.backgroundColor = UIColor.primaryColor()
            $0.layer.borderColor = UIColor.lightGray.cgColor
            $0.layer.cornerRadius = 4.0
        })
        
        backButton.then({
            $0.setTitleColor(.white, for: .normal)
            $0.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .regular)
            $0.titleLabel?.textAlignment = .center
            $0.backgroundColor = UIColor.primaryColor()
        })
        
        labelView.addSubview(backButton)
        backButton.snp.makeConstraints({
            $0.top.left.equalToSuperview().offset(6)
            $0.bottom.right.equalToSuperview().inset(6)
        })
        
        self.view.addSubview(labelView)
        labelView.snp.makeConstraints({
            $0.left.equalToSuperview().inset(20)
            $0.top.equalToSuperview().inset(40)
            $0.height.equalTo(btnHeight)
        })
    }
    
    func addLanguageButton() {
        let labelView = UIView().then({
            $0.backgroundColor = UIColor.primaryColor()
            $0.layer.borderColor = UIColor.lightGray.cgColor
            $0.layer.cornerRadius = 4.0
        })
        
        changeLanguageLbl.then({
            $0.textColor = .white
            $0.text = LangSetting.lang.rawValue.uppercased()
            $0.font = UIFont.systemFont(ofSize: 14, weight: .bold)
            $0.textAlignment = .center
            $0.backgroundColor = UIColor.primaryColor()
        })
        
        labelView.addSubview(changeLanguageLbl)
        changeLanguageLbl.snp.makeConstraints({
            $0.top.left.equalToSuperview().offset(6)
            $0.bottom.right.equalToSuperview().inset(6)
        })
        
        self.view.addSubview(labelView)
        labelView.snp.makeConstraints({
            $0.right.equalToSuperview().inset(20)
            $0.top.equalToSuperview().inset(40)
            $0.width.height.equalTo(btnHeight)
        })
        
        let ges = UITapGestureRecognizer(target: self, action: #selector(changeLanguage))
        changeLanguageLbl.addGestureRecognizer(ges)
        labelView.addGestureRecognizer(ges)
    }
    
    static func addPresentAnimation() -> CATransition {
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        return transition
    }
    
    @objc
    func backTapped() {
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        self.removeAllLayerAnimation()
        self.view.window!.layer.add(transition, forKey: nil)
        self.dismiss(animated: false, completion: {self.removeAllLayerAnimation()})
    }
    
    @objc
    func changeLanguage(_ isReloadWoutChange: Bool = false) {
        LangSetting.changeLange()
        self.changeLanguageLbl.text = LangSetting.lang.rawValue.uppercased()
        self.updateTextFieldLanguage()
    }
    
    func updateTextFieldLanguage() {
        
    }
    
    func reloadVC() {
        
    }
}
